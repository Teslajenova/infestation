﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Ai.Aids;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks {
    class AskForFood : SerialTask {

        //private Task subTask;

        private Map map;
        private PerceivedTileFinder perceivedTileFinder;
        private List<MapTile.TileType> traversibleTileTypes = new List<MapTile.TileType>();

        public AskForFood(Map map) {
            this.map = map;

            traversibleTileTypes.Add(MapTile.TileType.OPEN);
            traversibleTileTypes.Add(MapTile.TileType.EXIT);
        }

        protected override bool Init(GameTime gameTime) {

            // shall we use a perceived tile finder?
            this.perceivedTileFinder = new PerceivedTileFinder(
                map,
                hostCreature.GetCurrentTile(),
                traversibleTileTypes,
                hostCreature.GetPerception());

            return true;
        }

        protected override bool ClearToExecute() {

            return true;
        }
    }
}
