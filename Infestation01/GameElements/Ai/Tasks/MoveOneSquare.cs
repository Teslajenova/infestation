﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.Engine;
using Infestation01.GameElements.Maps;


namespace Infestation01.GameElements.Ai.Tasks
{
    class MoveOneSquare : Task
    {

        MapTile targetTile;

        Map map;

        Map.Direction direction;

        float delayer = 1;
        float delayTarget = 0;

        float waitCounter = 0f;
        int waitedTotal = 0;

        List<MapTile.TileType> types = new List<MapTile.TileType>();
                

        public MoveOneSquare(Map map, Map.Direction direction, Creature host)
        {
            this.hostCreature = host;
            this.map = map;
            this.direction = direction;

            types.Add(MapTile.TileType.OPEN);
            types.Add(MapTile.TileType.EXIT);
            
        }

        protected override Boolean Init(GameTime gameTime)
        {
            if (direction == Map.Direction.NONE)
            {
                unableToFinish = true;
                return false;
            }

            if(targetTile == hostCreature.GetCurrentTile())
                return true;

            if (this.targetTile == null)
            {
                this.targetTile = map.GetAdjacentTileIfCorrectType(hostCreature.GetCurrentTile(), this.direction, types);

                if (this.targetTile == null)
                {
                    unableToFinish = true;
                    return false;
                }
            }

            if (map.TileIsOccupied(targetTile))
            {
                Creature occupant = map.GetOccupyingCreature(targetTile);

                if (occupant != null && occupant.GetWaitingToEnter() == hostCreature.GetCurrentTile())
                {
                    occupant.SetCurrentTile(hostCreature.GetCurrentTile());
                    hostCreature.SetCurrentTile(targetTile);
                    return true;
                }
                else if (occupant != null && occupant.IsIdle()){
                    occupant.AddTask(new MoveOneRandomSquare(map, occupant), false);
                    return false;
                }
                else
                {
                    if (waitCounter <= 0f)
                    {
                        waitCounter += hostCreature.GetPatience() * 10;
                        waitedTotal++;

                        if (waitedTotal > hostCreature.GetPatience() / 2)
                        {
                            unableToFinish = true;
                            return false;
                        }

                        hostCreature.SetWaitingToEnter(targetTile);
                        return false;
                    }
                    else 
                    {
                        waitCounter -= gameTime.ElapsedGameTime.Milliseconds;
                        hostCreature.SetCurrentState(Creature.State.WAITING);
                        return false;
                    }
                }
            }
            else 
            {
                hostCreature.SetCurrentTile(targetTile);
                return true;
            }
                
        }

        protected override void DoTask(GameTime gameTime)
        {
            hostCreature.SetCurrentState(Creature.State.WALKING);

            float speedVar = (hostCreature.GetSpeed() * 0.00005f) * gameTime.ElapsedGameTime.Milliseconds;

            delayer -= speedVar;

            if (direction == Map.Direction.UP)
            {
                hostCreature.SetSpriteOffset(new Vector2(0, delayer));
            }
            else if (direction == Map.Direction.DOWN)
            {
                hostCreature.SetSpriteOffset(new Vector2(0, 0 - delayer));
            }
            else if (direction == Map.Direction.LEFT)
            {
                hostCreature.SetSpriteOffset(new Vector2(delayer, 0));
            }
            else if (direction == Map.Direction.RIGHT)
            {
                hostCreature.SetSpriteOffset(new Vector2(0 - delayer, 0));
            }

            if (delayer <= delayTarget)
            {
                hostCreature.SetSpriteOffset(new Vector2(0, 0));
                hostCreature.SetCurrentState(Creature.State.IDLE);
                done = true;
            }

        }

        
    }
}
