﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks
{
    class MoveOneRandomSquare : Task
    {

        private MapTile targetTile;

        private Map map;

        float progress;

        private Task subTask;

        public MoveOneRandomSquare(Map map, Creature host)
        {
            this.hostCreature = host;
            this.map = map;
        }

        protected override Boolean Init(GameTime gameTime)
        {
            List<MapTile.TileType> traversibleTypes = new List<MapTile.TileType>();
            traversibleTypes.Add(MapTile.TileType.OPEN);
            traversibleTypes.Add(MapTile.TileType.EXIT);

            List<MapTile> potentialTargets = map.GetTileNeighbours(hostCreature.GetCurrentTile(), traversibleTypes);

            List<Map.Direction> potentialDirections = new List<Map.Direction>();

            foreach (MapTile tile in potentialTargets) {

                if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() + 1) {
                    potentialDirections.Add(Map.Direction.RIGHT);
                    if (!map.TileIsOccupied(tile))
                        potentialDirections.Add(Map.Direction.RIGHT);
                }
                else if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() - 1) {
                    potentialDirections.Add(Map.Direction.LEFT);
                    if (!map.TileIsOccupied(tile))
                        potentialDirections.Add(Map.Direction.LEFT);
                }
                else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() + 1) {
                    potentialDirections.Add(Map.Direction.DOWN);
                    if (!map.TileIsOccupied(tile))
                        potentialDirections.Add(Map.Direction.DOWN);
                }
                else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() - 1) {
                    potentialDirections.Add(Map.Direction.UP);
                    if (!map.TileIsOccupied(tile))
                        potentialDirections.Add(Map.Direction.UP);
                }

                
            }

            Random r = Game1.random; //new Random();
            
            int index = r.Next(potentialDirections.Count);

            this.subTask = new MoveOneSquare(map, potentialDirections.ElementAt(index), hostCreature);

            //this.targetTile = potentialTargets[index];

            //progress = 0f;

            return true;
        }

        protected override void DoTask(GameTime gameTime)
        {
            this.subTask.Update(gameTime);
            this.unableToFinish = subTask.UnableToFinish();
            this.done = subTask.IsDone();

            /*
            if (this.hostCreature.GetCurrentTile() != targetTile)
            {
                this.hostCreature.SetCurrentTile(targetTile);
            }

            if (progress < 100)
            {

            }
            else 
            {
                done = true;
            }*/

            //this.hostCreature.currentTile = targetTile;
            //done = true;
        }

        
    }
}
