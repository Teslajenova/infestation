﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Maps;
using Infestation01.GameElements.Ai.Aids;
using Microsoft.Xna.Framework;

namespace Infestation01.GameElements.Ai.Tasks {
    class GotoNearestMoogle : Task {

        private Map map;

        private PerceivedTileFinder perceivedTileFinder;
        private ClosestMoogleFinder closestMoogleFinder;
        private PathFinder pathFinder;

        private List<MapTile.TileType> acceptableTileTypes;

        private List<MapTile> path;

        public GotoNearestMoogle(Map map, Creature host) {
            this.hostCreature = host;
            this.map = map;

            acceptableTileTypes = new List<MapTile.TileType>();
            acceptableTileTypes.Add(MapTile.TileType.OPEN);
            acceptableTileTypes.Add(MapTile.TileType.EXIT);
        }

        protected override bool Init(GameTime gameTime) {
            
            //perceivedTileFinder = new PerceivedTileFinder(map,

            return true;
        }

        protected override void DoTask(GameTime gameTime) {
            if (perceivedTileFinder == null)
                perceivedTileFinder = new PerceivedTileFinder(map, hostCreature.GetCurrentTile(), acceptableTileTypes, 100);

            if (closestMoogleFinder == null)
                closestMoogleFinder = new ClosestMoogleFinder(map, (Moogle)(hostCreature));

            perceivedTileFinder.Traverse();

            List<MapTile> moogleTiles = closestMoogleFinder.MoogleTiles(perceivedTileFinder.GetCurrentlyPerceivedTiles());

            //MapTile closestMoogleTile = 
              //  closestMoogleFinder.ClosestMoogleTile(perceivedTileFinder.GetCurrentlyPerceivedTiles(), hostCreature.GetCurrentTile());
            
            if (moogleTiles != null && moogleTiles.Count > 0) {

                //if (pathFinder == null)
                  //  pathFinder = new PathFinder(hostCreature, map, moogleTiles);
            }

            /*
            if (pathFinder != null) {
                path = pathFinder.FindPath();
            }

            if (path != null) {
                //????
            }*/

        }

    }
}
