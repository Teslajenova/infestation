﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Items;

namespace Infestation01.GameElements.Ai.Tasks
{
    class ConsumeItems : Task
    {

        //private Item mealItem;

        //private float eatingCounter;

        //private float eatingCounterMax = 200f;

        Task subTask;

        public ConsumeItems(Creature host)
        {
            this.hostCreature = host;
        }

        protected override bool Init(GameTime gameTime)
        {
            return true;
        }

        protected override void DoTask(GameTime gameTime)
        {

            if (!hostCreature.IsHungry(true))
            {
                done = true;
                return;
            }

            if (subTask == null)
            {
                List<Item> edibles = hostCreature.GetItems().GetAllEdibles();

                if (edibles.Count == 0)
                {
                    done = true;
                    return;
                }

                int randomIndex = Game1.random.Next(edibles.Count);

                Item mealItem = edibles.ElementAt(randomIndex);
                //hostCreature.GetItems().RemoveItem(mealItem);

                subTask = new EatItem(hostCreature, mealItem);

                //eatingCounter = eatingCounterMax;
            }

            if (subTask != null) {
                subTask.Update(gameTime);

                if (subTask.IsDone() || subTask.UnableToFinish()) {
                    subTask = null;
                }
            }

            /*
            if (mealItem != null && eatingCounter > 0)
            {
                if (eatingCounter == eatingCounterMax)
                    hostCreature.SetCurrentState(Creature.State.EATING);

                eatingCounter -= (gameTime.ElapsedGameTime.Milliseconds * 0.04f);

                if (eatingCounter < 0)
                {
                    hostCreature.SetCurrentState(Creature.State.IDLE);
                    hostCreature.ConsumeItem(mealItem);
                    mealItem = null;
                }
            }*/
        }

        
    }
}
