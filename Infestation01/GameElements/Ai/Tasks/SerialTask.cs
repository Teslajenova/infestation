﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;

using Infestation01.Engine;
using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Ai.Tasks
{
    abstract class SerialTask : Task
    {
        protected List<Task> subTasks = new List<Task>();
        Task currentSubTask;

        

        protected override void DoTask(GameTime gameTime)
        {
            // enable preemptive behaviour !!!!
            if (ClearToExecute()) {

                if (currentSubTask != null && currentSubTask.UnableToFinish()) {
                    if (UnableToFinishDueToFailedSubTask()) 
                        unableToFinish = true;
                        //done = true;
                    else
                        currentSubTask = null;
                }

                if (currentSubTask != null && currentSubTask.IsDone()) {
                    currentSubTask = null;
                }

                if (currentSubTask == null && subTasks.Count > 0) {
                    currentSubTask = subTasks.ElementAt(0);
                    subTasks.Remove(currentSubTask);
                }

                if (currentSubTask == null && subTasks.Count == 0) {
                    done = true;
                }

                if (done == false) {
                    currentSubTask.Update(gameTime);
                }
            }
        }

        protected virtual bool ClearToExecute() {
            return true;
        }

        protected virtual bool UnableToFinishDueToFailedSubTask()
        {
            return true;
        }

        public override void DebugDraw(SpriteBatch spriteBatch, Camera camera) {
            if (currentSubTask != null)
                currentSubTask.DebugDraw(spriteBatch, camera);
        }
    }
}
