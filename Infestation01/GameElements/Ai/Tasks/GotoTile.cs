﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.Engine;
using Infestation01.GameElements.Ai.Aids;
using Infestation01.GameElements.Maps;

using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Ai.Tasks
{
    class GotoTile : Task
    {
        List<MapTile> goalTiles;
        Map map;

        PathFinder pathFinder;

        List<MapTile> chosenPath = null; //new List<MapTile>();

        Task subTask;

        private int waypointFrequency;

        public GotoTile(Map map, List<MapTile> goalTiles, Creature host, int waypointFrequency)
        {
            this.hostCreature = host;
            this.map = map;
            this.goalTiles = goalTiles;
            this.waypointFrequency = waypointFrequency;

            //pathFinder = new PathFinder(host, map, goalTiles, this.waypointFrequency); //init in doTask?

        }

        protected override Boolean Init(GameTime gameTime)
        {
            //chosenPath = pathFinder.FindPath(goalTiles);

            if (goalTiles == null || goalTiles.Count == 0) {
                this.unableToFinish = true;
            }

            return true;
        }

        protected override void DoTask(GameTime gameTime)
        {
            // are we there yet?
            if (subTask == null && goalTiles.Any(tile => tile == hostCreature.GetCurrentTile())) {
                this.done = true;
                return;
            }

            if (subTask != null) {
                if (subTask.UnableToFinish()) {
                    subTask = null;
                    chosenPath = null;
                    pathFinder.Reset();

                    return;
                }

                if (!subTask.IsDone()) {
                    subTask.Update(gameTime);

                    return;
                }
                else {
                    subTask = null;
                }
            }
                
            // setup pathfinder
            if (pathFinder == null)
                pathFinder = new PathFinder(hostCreature, map, goalTiles, waypointFrequency);

            // find path, or partial path
            if (chosenPath == null) {
                hostCreature.SetCurrentState(Creature.State.THINKING);
                chosenPath = pathFinder.FindPath();
            }

            // when path is chosen, go there
            if (chosenPath != null) {

                // get next step
                if (chosenPath.Count > 0) {
                    MapTile tile = chosenPath.Last();
                    chosenPath.Remove(tile);

                    Map.Direction direction = Map.Direction.NONE;

                    if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() - 1)
                        direction = Map.Direction.LEFT;
                    else if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() + 1)
                        direction = Map.Direction.RIGHT;
                    else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() - 1)
                        direction = Map.Direction.UP;
                    else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() + 1)
                        direction = Map.Direction.DOWN;

                    if (direction != Map.Direction.NONE) {
                        subTask = new MoveOneSquare(map, direction, hostCreature);
                    }
                }
                else {
                    subTask = null;
                    chosenPath = null;
                    pathFinder.Reset();
                }
            }

            /*
            if (chosenPath == null) {
                hostCreature.SetCurrentState(Creature.State.THINKING);

                chosenPath = pathFinder.FindPath();
            }

            if (subTask == null && chosenPath != null && chosenPath.Count > 0)
            {
                MapTile tile = chosenPath.Last();
                chosenPath.Remove(tile);

                Map.Direction direction = Map.Direction.NONE;

                if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() -1)
                    direction = Map.Direction.LEFT;
                else if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() +1)
                    direction = Map.Direction.RIGHT;
                else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() -1)
                    direction = Map.Direction.UP;
                else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() +1)
                    direction = Map.Direction.DOWN;

                if (direction != Map.Direction.NONE)
                {
                    subTask = new MoveOneSquare(map, direction, hostCreature);
                }
            }

            if (subTask != null) {
                if (subTask.UnableToFinish()) {
                    subTask = null;
                    chosenPath = null;
                    pathFinder = new PathFinder(hostCreature, map, goalTiles, waypointFrequency);

                    return;
                }

                if (!subTask.IsDone()) {
                    subTask.Update(gameTime);
                }
                else {
                    subTask = null;
                }

            }
            else {
                hostCreature.SetCurrentState(Creature.State.THINKING);
            }

            if (subTask == null && chosenPath != null && chosenPath.Count == 0)
            {
                foreach (MapTile goalTile in goalTiles)
                {
                    if (hostCreature.GetCurrentTile() == goalTile)
                    {
                        done = true;
                        return;
                    }
                }

                chosenPath = null;
                pathFinder = new PathFinder(hostCreature, map, goalTiles, waypointFrequency);
            }
            */
        }

        public override void DebugDraw(SpriteBatch spriteBatch, Camera camera) {

            

            if (chosenPath != null) {
                foreach (MapTile tile in chosenPath) {
                    Vector2 drawPos = new Vector2(
                         (this.map.GetPosition().X + this.map.GetTileSize().X * tile.GetCoordinateX()) - camera.GetPosition().X,
                        //+ map.GetTileSize().X / 2,  // center sprite
                        //+ map.GetTileSize().X * spriteOffset.X,    // offset
                         (this.map.GetPosition().Y + this.map.GetTileSize().Y * tile.GetCoordinateY()) - camera.GetPosition().Y);
                         //+ map.GetTileSize().Y / 2);  // center sprite
                    //+ map.GetTileSize().Y * spriteOffset.Y);   // offset

                    spriteBatch.DrawString(Game1.graphLib.GetDefFont(),
                        "CC",
                        drawPos,
                        Color.LightSteelBlue);
                }
            }

            if (pathFinder != null)
                pathFinder.DebugDraw(spriteBatch, camera);

            /*
            List<PathStep> closedSet = this.pathFinder.GetClosedSet();

            PathStep bestStep = null;

            //closedSet.OrderBy(

            foreach (PathStep step in closedSet){
                if (bestStep == null) {
                    bestStep = step;
                }
                else if (step.GetTotalValue() < bestStep.GetTotalValue()) {
                    bestStep = step;
                }
            }

            while (bestStep != null) {
                Vector2 drawPos = new Vector2(
                     (this.map.GetPosition().X + this.map.GetTileSize().X * bestStep.GetTile().GetCoordinateX()) - camera.GetPosition().X
                     + map.GetTileSize().X / 2,  // center sprite
                     //+ map.GetTileSize().X * spriteOffset.X,    // offset
                     (this.map.GetPosition().Y + this.map.GetTileSize().Y * bestStep.GetTile().GetCoordinateY()) - camera.GetPosition().Y
                     + map.GetTileSize().Y / 2);  // center sprite
                     //+ map.GetTileSize().Y * spriteOffset.Y);   // offset

                spriteBatch.DrawString(Game1.graphLib.GetDefFont(),
                    "CC",
                    drawPos,
                    Color.LightSteelBlue);

                bestStep = bestStep.GetPreviousStep();
            }
             * */
        }
    }
}
