﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Items;

namespace Infestation01.GameElements.Ai.Tasks
{
    class EatItem : Task
    {

        private Item mealItem;

        private float eatingCounter;

        private float eatingCounterMax = 200f;

        public EatItem(Creature host, Item item)
        {
            this.hostCreature = host;

            if (hostCreature.GetItems().Contains(item)) {
                mealItem = item;
                //hostCreature.GetItems().RemoveItem(item);
            }
                

            eatingCounter = eatingCounterMax;
        }

        protected override bool Init(GameTime gameTime)
        {
            return true;
        }

        protected override void DoTask(GameTime gameTime)
        {
            /*
            if (!hostCreature.IsHungry(true))
            {
                done = true;
                return;
            }*/

            if (mealItem == null)
            {
                this.unableToFinish = true;
                return;
                /*
                List<Item> edibles = hostCreature.GetItems().GetAllEdibles();

                if (edibles.Count == 0)
                {
                    done = true;
                    return;
                }

                int randomIndex = Game1.random.Next(edibles.Count);

                mealItem = edibles.ElementAt(randomIndex);
                hostCreature.GetItems().RemoveItem(mealItem);

                eatingCounter = eatingCounterMax;
                 * */
            }

            if (eatingCounter == eatingCounterMax)
                hostCreature.SetCurrentState(Creature.State.EATING);

            eatingCounter -= (gameTime.ElapsedGameTime.Milliseconds * 0.04f);

            if (eatingCounter < 0)
            {
                hostCreature.SetCurrentState(Creature.State.IDLE);
                hostCreature.ConsumeItem(mealItem);
                this.done = true;
            }
            
        }

        
    }
}
