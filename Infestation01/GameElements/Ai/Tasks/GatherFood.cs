﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks {
    class GatherFood : SerialTask {

        Map map;

        public GatherFood(Creature host, Map map) {
            this.hostCreature = host;
            this.map = map;
        }

        protected override bool Init(GameTime gameTime) {
            // find food sources

            List<MapTile> goalTiles = map.GetNearbyTilesWithFood(hostCreature);
            if (goalTiles != null && goalTiles.Count > 0) {
                subTasks.Add(new GotoTile(map, goalTiles, hostCreature, 0));
                subTasks.Add(new PicUpFood(hostCreature));
            }
            else {
                this.unableToFinish = true;
            }

            return true;
        }


    }
}
