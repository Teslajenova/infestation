﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks
{
    class Exit : Task
    {

        public Exit(Creature host)
        {
            this.hostCreature = host;
        }

        protected override Boolean Init(GameTime gameTime)
        {
            if (hostCreature.GetCurrentTile().GetTileType() == MapTile.TileType.EXIT)
            {
                return true;
            }
            else 
            {
                unableToFinish = true;
                return false;
            }
        }

        protected override void DoTask(GameTime gameTime)
        {
            hostCreature.SetOffMap(true);
        }
    }
}
