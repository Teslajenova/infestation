﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Ai.Aids;
using Infestation01.GameElements.Maps;

using Infestation01.Engine;
using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Ai.Tasks {
    class GatherFood3 : SerialTask {

        Map map;

        private PerceivedTileFinder perceivedTileFinder;
        private List<MapTile.TileType> traversibleTileTypes = new List<MapTile.TileType>();

        private bool subTasksAdded = false;

        public GatherFood3(Creature host, Map map) {
            this.hostCreature = host;
            this.map = map;

            traversibleTileTypes.Add(MapTile.TileType.OPEN);
            traversibleTileTypes.Add(MapTile.TileType.EXIT);
        }

        protected override bool Init(GameTime gameTime) {
            this.perceivedTileFinder = new PerceivedTileFinder(
                map,
                hostCreature.GetCurrentTile(),
                traversibleTileTypes,
                hostCreature.GetPerception());

            return true;

            /*
            // find food sources

            List<MapTile> goalTiles = map.GetNearbyTilesWithFood(hostCreature);
            if (goalTiles != null && goalTiles.Count > 0) {
                subTasks.Add(new GotoTile(map, goalTiles, hostCreature));
                subTasks.Add(new PicUpFood(hostCreature));
            }
            else {
                this.unableToFinish = true;
            }

            return true;
             * */
        }

        protected override bool ClearToExecute() {

            if (subTasksAdded)
                return true;

            if (!perceivedTileFinder.IsFinished()) {
                perceivedTileFinder.Traverse();
                hostCreature.SetCurrentState(Creature.State.THINKING);
            }

            if (perceivedTileFinder.IsFinished()) {
                List<MapTile> tilesWithFood = 
                    perceivedTileFinder.GetCurrentlyPerceivedTiles().FindAll(tile => tile.GetItems().HasEdibles());

                subTasks.Add(new GotoTile(map, tilesWithFood, hostCreature, 0));
                subTasks.Add(new PicUpFood(hostCreature));
                subTasksAdded = true;
                return true;
            }

            return false;
        }

        public override void DebugDraw(SpriteBatch spriteBatch, Camera camera) {
            if (subTasksAdded) {
                base.DebugDraw(spriteBatch, camera);
            }
            else if (perceivedTileFinder != null) {
                foreach (MapTile tile in perceivedTileFinder.GetCurrentlyPerceivedTiles()) {
                    Vector2 drawPos = new Vector2(
                     (this.map.GetPosition().X + this.map.GetTileSize().X * tile.GetCoordinateX()) - camera.GetPosition().X
                     + map.GetTileSize().X / 2,  // center sprite
                     //+ map.GetTileSize().X * spriteOffset.X,    // offset
                     (this.map.GetPosition().Y + this.map.GetTileSize().Y * tile.GetCoordinateY()) - camera.GetPosition().Y
                     + map.GetTileSize().Y / 2);  // center sprite
                     //+ map.GetTileSize().Y * spriteOffset.Y);   // offset

                    spriteBatch.DrawString(
                        Game1.graphLib.GetDefFont(),
                        "AA",
                        drawPos,
                        Color.GhostWhite);
                }
            }
        } 
 

    }
}
