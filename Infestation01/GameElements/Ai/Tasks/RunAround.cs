﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks
{
    class RunAround : SerialTask
    {
        Map map;

        public RunAround(Map map, Creature host)
        {
            this.hostCreature = host;
            this.map = map;
        }

        protected override Boolean Init(GameTime gameTime)
        {
            Random random = Game1.random;

            for (int i = 0; i < 1000; i++)
            {
                //subTasks.Add(new MoveOneRandomSquare(map, hostCreature));

                Map.Direction direction = Map.Direction.UP;

                int r = random.Next(4);

                if (r == 0)
                {
                    direction = Map.Direction.UP;
                }
                else if (r == 1)
                {
                    direction = Map.Direction.DOWN;
                }
                else if (r == 2)
                {
                    direction = Map.Direction.LEFT;
                }
                else if (r == 3)
                {
                    direction = Map.Direction.RIGHT;
                }

                subTasks.Add(new MoveOneSquare(map, direction, hostCreature));
            }

            return true;
        }

        protected virtual bool UnableToFinishDueToFailedTask()
        {
            return false;
        }


    }
}
