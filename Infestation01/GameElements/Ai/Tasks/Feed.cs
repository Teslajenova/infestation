﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Items;
using Infestation01.GameElements.Maps;

using Infestation01.Engine;
using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Ai.Tasks
{
    class Feed : Task
    {

        private Map map;

        Task subTask;

        private bool hasGatheredFood = false;

        public Feed(Creature host, Map map)
        {
            this.hostCreature = host;
            this.map = map;
        }

        protected override bool Init(GameTime gameTime)
        {
            return true; // ??
        }

        protected override void DoTask(GameTime gameTime)
        {
            if (subTask != null)
            {
                if (subTask.UnableToFinish() || subTask.IsDone())
                {
                    subTask = null;
                }
                else
                {
                    subTask.Update(gameTime);
                }

            }           

            if (subTask == null)
            {
                if (!hostCreature.IsHungry(false))
                {
                    this.done = true;
                    return;
                }

                if (hostCreature.GetItems().HasEdibles())
                {
                    subTask = new ConsumeItems(hostCreature);
                }
                else if (!hasGatheredFood) {
                    subTask = new GatherFood3(hostCreature, map);
                    hasGatheredFood = true;
                    //this.unableToFinish = true;
                }
                else {
                    subTask = new GoOut(hostCreature, map);
                    //this.unableToFinish = true;
                }

                
            }

            
        }


        public override void DebugDraw(SpriteBatch spriteBatch, Camera camera) {
            if (subTask != null)
                subTask.DebugDraw(spriteBatch, camera);
        }
    }
}
