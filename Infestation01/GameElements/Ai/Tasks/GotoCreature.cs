﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.Engine;
using Infestation01.GameElements.Ai.Aids;
using Infestation01.GameElements.Maps;

using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Ai.Tasks
{
    class GotoCreature : Task
    {
        private Creature targetCreature;
        private MapTile temporaryTargetTile;

        private Map map;

        private PathFinder pathFinder;

        private List<MapTile> chosenPath = null;

        private Task subTask;

        private int waypointFrequency;

        public GotoCreature(Map map, Creature targetCreature, Creature host, int waypointFrequency)
        {
            this.hostCreature = host;
            this.map = map;
            this.targetCreature = targetCreature;
            this.temporaryTargetTile = targetCreature.GetCurrentTile();
            this.waypointFrequency = waypointFrequency;

        }

        protected override Boolean Init(GameTime gameTime) {
            if (targetCreature == null) {
                this.unableToFinish = true;
            }

            return true;
        }

        protected override void DoTask(GameTime gameTime)
        {
            if (subTask == null) {
                // update target location
                MapTile targetCurrentTile = targetCreature.GetCurrentTile();
                if (temporaryTargetTile != targetCurrentTile) {
                    List<MapTile> goalTiles = new List<MapTile>();
                    goalTiles.Add(targetCurrentTile);
                    pathFinder.UpdateGoalTiles(goalTiles);
                    temporaryTargetTile = targetCurrentTile;
                }

                // are we there yet?
                if (chosenPath != null) {
                    List<MapTile> targetTiles = map.GetTileNeighbours(targetCreature.GetCurrentTile());
                    foreach (MapTile tile in targetTiles) {
                        if (hostCreature.GetCurrentTile() == tile) {
                            this.done = true;
                            return;
                        }
                    }
                }
            }

            /*
            if (subTask == null && goalTiles.Any(tile => tile == hostCreature.GetCurrentTile())) {
                this.done = true;
                return;
            }*/

            if (subTask != null) {
                if (subTask.UnableToFinish()) {
                    subTask = null;
                    chosenPath = null;
                    List<MapTile> goalTiles = new List<MapTile>();
                    goalTiles.Add(targetCreature.GetCurrentTile());
                    pathFinder.Reset(goalTiles);

                    return;
                }

                if (!subTask.IsDone()) {
                    subTask.Update(gameTime);

                    return;
                }
                else {
                    subTask = null;
                }
            }
                
            // setup pathfinder
            if (pathFinder == null) {
                List<MapTile> goalTiles = new List<MapTile>();
                goalTiles.Add(targetCreature.GetCurrentTile());
                pathFinder = new PathFinder(hostCreature, map, goalTiles, waypointFrequency);
            }

            // find path, or partial path
            if (chosenPath == null) {
                hostCreature.SetCurrentState(Creature.State.THINKING);
                chosenPath = pathFinder.FindPath();
            }

            // when path is chosen, go there
            if (chosenPath != null) {

                // get next step
                if (chosenPath.Count > 0) {
                    MapTile tile = chosenPath.Last();
                    chosenPath.Remove(tile);

                    Map.Direction direction = Map.Direction.NONE;

                    if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() - 1)
                        direction = Map.Direction.LEFT;
                    else if (tile.GetCoordinateX() == hostCreature.GetCurrentTile().GetCoordinateX() + 1)
                        direction = Map.Direction.RIGHT;
                    else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() - 1)
                        direction = Map.Direction.UP;
                    else if (tile.GetCoordinateY() == hostCreature.GetCurrentTile().GetCoordinateY() + 1)
                        direction = Map.Direction.DOWN;

                    if (direction != Map.Direction.NONE) {
                        subTask = new MoveOneSquare(map, direction, hostCreature);
                    }
                }
                else {
                    subTask = null;
                    chosenPath = null;
                    List<MapTile> goalTiles = new List<MapTile>();
                    goalTiles.Add(targetCreature.GetCurrentTile());
                    pathFinder.Reset(goalTiles);
                }
            }
        }

        public override void DebugDraw(SpriteBatch spriteBatch, Camera camera) {
            if (chosenPath != null) {
                foreach (MapTile tile in chosenPath) {
                    Vector2 drawPos = new Vector2(
                         (this.map.GetPosition().X + this.map.GetTileSize().X * tile.GetCoordinateX()) - camera.GetPosition().X,
                        //+ map.GetTileSize().X / 2,  // center sprite
                        //+ map.GetTileSize().X * spriteOffset.X,    // offset
                         (this.map.GetPosition().Y + this.map.GetTileSize().Y * tile.GetCoordinateY()) - camera.GetPosition().Y);
                         //+ map.GetTileSize().Y / 2);  // center sprite
                    //+ map.GetTileSize().Y * spriteOffset.Y);   // offset

                    spriteBatch.DrawString(Game1.graphLib.GetDefFont(),
                        "CC",
                        drawPos,
                        Color.LightSteelBlue);
                }
            }

            if (pathFinder != null)
                pathFinder.DebugDraw(spriteBatch, camera);

        }
    }
}
