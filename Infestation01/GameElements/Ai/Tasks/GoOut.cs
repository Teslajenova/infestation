﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Ai;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks
{
    class GoOut : SerialTask
    {

        Map map;

        public GoOut(Creature host, Map map)
        {
            this.hostCreature = host;

            this.map = map;
        }

        protected override Boolean Init(GameTime gameTime)
        {
            List<MapTile> exits = map.GetAllTilesOfType(MapTile.TileType.EXIT);

            subTasks.Add(new GotoTile(map, exits, hostCreature, 0));
            subTasks.Add(new Exit(hostCreature));

            return true;
        }

    }
}
