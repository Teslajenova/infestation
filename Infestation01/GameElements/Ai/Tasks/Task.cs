﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Items;

using Microsoft.Xna.Framework.Graphics;
using Infestation01.Engine;

namespace Infestation01.GameElements.Ai.Tasks
{
    abstract class Task
    {
        protected Creature hostCreature;

        protected bool done = false;
        protected bool initialized = false;
        protected bool unableToFinish = false;


        public void Update(GameTime gameTime)
        {
            // unable to finish???
            if (!unableToFinish)
            {
                if (!initialized)
                {
                    initialized = Init(gameTime);
                }

                if (initialized && !done && !unableToFinish)
                {
                    DoTask(gameTime);
                }
            }
        }

        protected abstract Boolean Init(GameTime gameTime);

        protected abstract void DoTask(GameTime gameTime);

        public Boolean IsDone()
        {
            return done;
        }

        public bool UnableToFinish()
        {
            return unableToFinish;
        }

        public virtual String TaskType()
        {
            return this.ToString();
        }

        public virtual void DebugDraw(SpriteBatch spriteBatch, Camera camera) {

        }

    }
}
