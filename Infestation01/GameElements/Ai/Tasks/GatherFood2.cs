﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Ai.Aids;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Tasks {
    class GatherFood2 : Task {

        private Map map;

        private Task subTaskGotoTile;
        private Task subTaskPickUpFood;

        private PerceivedTileFinder perceivedTileFinder;
        private List<MapTile> tilesWithFood = new List<MapTile>();

        private List<MapTile.TileType> traversibleTileTypes = new List<MapTile.TileType>();



        public GatherFood2(Creature host, Map map) {

            this.hostCreature = host;
            this.map = map;

            traversibleTileTypes.Add(MapTile.TileType.OPEN);
            traversibleTileTypes.Add(MapTile.TileType.EXIT);
        }

        protected override bool Init(GameTime gameTime) {

            this.perceivedTileFinder = new PerceivedTileFinder(
                map, 
                hostCreature.GetCurrentTile(), 
                traversibleTileTypes, 
                hostCreature.GetPerception());

            return true;

            /*
            // find food sources

            List<MapTile> goalTiles = map.GetNearbyTilesWithFood(hostCreature);
            if (goalTiles != null && goalTiles.Count > 0) {
                subTasks.Add(new GotoTile(map, goalTiles, hostCreature));
                subTasks.Add(new PicUpFood(hostCreature));
            }
            else {
                this.unableToFinish = true;
            }

            return true;
             * */
        }

        protected override void DoTask(GameTime gameTime) {
            if (!perceivedTileFinder.IsFinished()) {
                perceivedTileFinder.Traverse();

                if (hostCreature.GetCurrentState() != Creature.State.THINKING) {
                    hostCreature.SetCurrentState(Creature.State.THINKING);
                }

                if (perceivedTileFinder.IsFinished()) {
                    subTaskGotoTile = new GotoTile(map, perceivedTileFinder.GetCurrentlyPerceivedTiles(), hostCreature, 0);
                }
            }

            if (perceivedTileFinder.IsFinished()) {
                if (!subTaskGotoTile.IsDone()) {
                    subTaskGotoTile.Update(gameTime);
                }
                else {
                    subTaskPickUpFood = new PicUpFood(hostCreature);
                }

                if (subTaskPickUpFood != null) {
                    if (subTaskPickUpFood != null && !subTaskPickUpFood.IsDone()) {
                        subTaskPickUpFood.Update(gameTime);
                    }

                    if (subTaskPickUpFood.IsDone()) {
                        done = true;
                    }

                    if (subTaskPickUpFood.UnableToFinish()) {
                        unableToFinish = true;
                    }
                }
                

                if (subTaskGotoTile.UnableToFinish()) {
                    unableToFinish = true;
                }

            }


            // resets tilesWithFood. could also just add to it...
            /*
            List<MapTile> currentPerceivedTilesWithFood = GetCurrentPerceivedTilesWithFood();

            if (currentPerceivedTilesWithFood.Count > tilesWithFood.Count) {
                tilesWithFood = currentPerceivedTilesWithFood;
            }*/

        }

        private List<MapTile> GetCurrentPerceivedTilesWithFood() {
            //List<MapTile> newtilesWithFood = new List<MapTile>();

            return perceivedTileFinder.GetCurrentlyPerceivedTiles().FindAll(tile => tile.GetItems().HasEdibles());

            /*
            foreach (MapTile tile in currentPerceivedTiles) {
                if (tile.GetItems().HasEdibles()) {
                    newtilesWithFood.Add(tile);
                }
            }*/
        }

    }
}
