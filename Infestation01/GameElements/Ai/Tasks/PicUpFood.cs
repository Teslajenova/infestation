﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;

namespace Infestation01.GameElements.Ai.Tasks {
    class PicUpFood : Task {

        float crouchingCounter;

        float crouchingCounterMax = 100f;

        public PicUpFood(Creature host) {
            hostCreature = host;
            crouchingCounter = crouchingCounterMax;
        }

        protected override bool Init(GameTime gameTime) {
            return true;
        }

        protected override void DoTask(GameTime gameTime) {

            hostCreature.SetCurrentState(Creature.State.CROUCHING);

            if (crouchingCounter == crouchingCounterMax) {

                hostCreature.GetItems().AddItem(
                    hostCreature.GetCurrentTile().GetItems().GetAllEdibles());
            }

            crouchingCounter -= (gameTime.ElapsedGameTime.Milliseconds * 0.1f);

            if (crouchingCounter < 0) {
                hostCreature.SetCurrentState(Creature.State.IDLE);
                this.done = true;
            }

        }
    }
}
