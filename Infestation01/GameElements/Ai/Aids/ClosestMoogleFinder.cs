﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Maps;
using Infestation01.GameElements.Creatures;

namespace Infestation01.GameElements.Ai.Aids {
    class ClosestMoogleFinder {

        private Map map;
        //private List<MapTile> visibleTiles;
        private Moogle referenceMoogle;


        public ClosestMoogleFinder(Map map, Moogle referenceMoogle) {
            this.map = map;
            this.referenceMoogle = referenceMoogle;
            //this.visibleTiles = new List<MapTile>();
        }

        public List<MapTile> MoogleTiles(List<MapTile> visibleTiles) {
            if (!MooglesInRange(visibleTiles))
                return null;

            return GetTilesWithMoogles(visibleTiles);
        }
        
        public MapTile ClosestMoogleTile(List<MapTile> visibleTiles, MapTile originTile) {
            //ResetVisibleTiles(visibleTiles);

            if (!MooglesInRange(visibleTiles))
                return null;

            List<MapTile> TilesWithMoogles = GetTilesWithMoogles(visibleTiles);

            if (TilesWithMoogles.Count == 1)
                return TilesWithMoogles.First();

            MapTile chosenMoogleTile = TilesWithMoogles.First();

            for (int i = 1; i < TilesWithMoogles.Count; i++) {
                MapTile tile = TilesWithMoogles.ElementAt(i);

                long chosenTileDist = 
                    Math.Abs(originTile.GetCoordinateX() - chosenMoogleTile.GetCoordinateX()) +
                    Math.Abs(originTile.GetCoordinateY() - chosenMoogleTile.GetCoordinateY());

                long tileDist =
                    Math.Abs(originTile.GetCoordinateX() - tile.GetCoordinateX()) +
                    Math.Abs(originTile.GetCoordinateY() - tile.GetCoordinateY());

                if (tileDist < chosenTileDist)
                    chosenMoogleTile = tile;
            }

            return chosenMoogleTile;
        }

        private List<MapTile> GetTilesWithMoogles(List<MapTile> allTiles) {
            List<MapTile> mogTiles = new List<MapTile>();

            foreach (MapTile tile in allTiles) {
               Creature occupant = map.GetOccupyingCreature(tile);
               if (occupant != null && occupant.CreatureType().Equals(referenceMoogle.CreatureType()))
                   mogTiles.Add(tile);
            }

            return mogTiles;
        }

        private bool MooglesInRange(List<MapTile> range) {
            foreach (MapTile tile in range) {
                Creature occupant = map.GetOccupyingCreature(tile);
                if (occupant != null && occupant.CreatureType().Equals(referenceMoogle.CreatureType()))
                    return true;
            }

            return false;
        }

        /*
        private void ResetVisibleTiles(List<MapTile> newTiles) {
            this.visibleTiles = newTiles;
        }*/
    }
}
