﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.Engine;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Maps;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Infestation01.Engine;

namespace Infestation01.GameElements.Ai.Aids
{
    class PathFinder
    {
        static float CLOSE_TO_GOAL_VALUE = 3f;
        static float SEARCH_RANGE = 3f;
        static float LOWEST_SCORE;

        private List<MapTile> goalTiles;

        Map map;
        Creature creature;

        List<PathStep> fringe;
        List<PathStep> closedSet;

        //private bool pathFound = false;
        bool goalReached = false;

        private int waypointFrequency;
        private int wpFrequencyCounter = 0;

        public PathFinder(Creature creature, Map map, List<MapTile> goalTiles, int waypointFrequency)
        {
            this.creature = creature;
            this.map = map;
            this.goalTiles = goalTiles;
            this.waypointFrequency = waypointFrequency;

            fringe = new List<PathStep>();
            closedSet = new List<PathStep>();

            fringe.Add(new PathStep(creature.GetCurrentTile(), creature.GetCurrentTile(), goalTiles, null, map));
        }

        public void Reset() {
            fringe = new List<PathStep>();
            closedSet = new List<PathStep>();
            wpFrequencyCounter = 0;
            goalReached = false;

            fringe.Add(new PathStep(creature.GetCurrentTile(), creature.GetCurrentTile(), goalTiles, null, map));
        }

        public void Reset(List<MapTile> goalTiles) {
            UpdateGoalTiles(goalTiles);
            Reset();
        }

        public void UpdateGoalTiles(List<MapTile> newGoals) {
            this.goalTiles = newGoals;
        }

        public List<MapTile> FindPath()
        {
            if (goalTiles.Count == 0)
                return null;

            // shortcut for standing on a goal
            if (goalTiles.Any(tile => tile == creature.GetCurrentTile()))
                return new List<MapTile>();

            

            //this.goalTiles = goalTiles;

            //fringe = new List<PathStep>();
            //closedSet = new List<PathStep>();

            //fringe.Add(new PathStep(creature.GetCurrentTile(), creature.GetCurrentTile(), goalTiles, null, map));

            //bool goalReached = false;

            //do
            //{

            if (!goalReached && (waypointFrequency == 0 || wpFrequencyCounter < waypointFrequency)) {

                // a step has reached goal -> we're done!
                List<PathStep> ReachedGoals = fringe.FindAll(PathStepIsGoal);
                if (ReachedGoals.Count > 0) {
                    MoveStepToClosedSet(ReachedGoals.ElementAt(0));
                    goalReached = true;
                    //break; // ????
                }
                else {
                    // close to goal: iterate from lowest value
                    List<PathStep> StepsNearGoal = fringe.FindAll(PathStepIsCloseToGoal);
                    if (StepsNearGoal.Count > 0) {
                        StepsNearGoal.Sort(
                            delegate(PathStep x, PathStep y) {
                                return x.GetTrueDistanceToGoal().CompareTo(y.GetTrueDistanceToGoal());
                            });

                        ExploreFringeStep(StepsNearGoal.ElementAt(0));
           
                    }
                    else {
                        // still looking: iterate the search range from highest down
                        LOWEST_SCORE = float.NaN;

                        foreach (PathStep pathStep in fringe) {
                            if (LOWEST_SCORE != LOWEST_SCORE || LOWEST_SCORE > pathStep.GetTotalValue()) // self comparison intentional (NaN)
                        {
                                LOWEST_SCORE = pathStep.GetTotalValue();
                            }
                        }

                        List<PathStep> stepsInSearchRange = fringe.FindAll(PathStepIsWithinSearchRange);
                        stepsInSearchRange.Sort(
                            delegate(PathStep x, PathStep y) {
                                return x.GetTotalValue().CompareTo(y.GetTotalValue());
                            });

                        ExploreFringeStep(stepsInSearchRange.ElementAt(0));
                    }

                }
            }
            //} while (!goalReached);


            if (goalReached) {
                return GetPathAsMapTiles();
            }
            else if(waypointFrequency > 0){
                wpFrequencyCounter++;
                if (wpFrequencyCounter >= waypointFrequency) {
                    //wpFrequencyCounter = 0; // ????
                    return GetPartialPathAsMapTiles();
                }

                return null;
            }
            else {
                return null;
            }
        }

        private List<MapTile> GetPartialPathAsMapTiles() {
            PathStep waypointStep = closedSet.Last();

            // this is still suspect...
            for (int i = closedSet.Count - 2; i >= 0; i--) {
                PathStep pathStep = closedSet.ElementAt(i);
                if (pathStep.GetTotalValue() < waypointStep.GetTotalValue())
                    waypointStep = pathStep;
            }
            /*
            foreach (PathStep pathStep in closedSet) {
                if (pathStep.GetTotalValue() < waypointStep.GetTotalValue())
                    waypointStep = pathStep;
            }*/

            List<MapTile> path = new List<MapTile>();

            PathStep step = closedSet.Last();

            do {
                path.Add(step.GetTile());
                step = step.GetPreviousStep();
            } while (step.GetPreviousStep() != null);

            //ResetStartingPoint(waypointStep);

            // ???? 
            //if (CLOSE_TO_GOAL_VALUE > 0f)
              //  CLOSE_TO_GOAL_VALUE -= 0.1f;

            return path;
        }

        /*
        private void ResetStartingPoint(PathStep newOriginStep) {
            closedSet = new List<PathStep>();
            fringe = new List<PathStep>();

            fringe.Add(new PathStep(newOriginStep.GetTile(), newOriginStep.GetTile(), goalTiles, null, map));
        }
         * */

        private List<MapTile> GetPathAsMapTiles()
        {
            List<MapTile> path = new List<MapTile>();

            PathStep step = closedSet.Last();

            do {
                path.Add(step.GetTile());
                step = step.GetPreviousStep();
            } while (step.GetPreviousStep() != null); // does this cause trouble?

            return path;
        }

        private void MoveStepToClosedSet(PathStep step)
        {
            if (fringe.Contains(step))
            {
                fringe.Remove(step);
                closedSet.Add(step);
            }

        }

        private void ExploreFringeStep(PathStep step)
        {
            if (!fringe.Contains(step))
            {
                return;
            }

            MoveStepToClosedSet(step);

            // add unknown neighbors to fringe
            List<MapTile.TileType> types = new List<MapTile.TileType>();
            types.Add(MapTile.TileType.OPEN);
            types.Add(MapTile.TileType.EXIT);
            List<MapTile> neighbors = map.GetTileNeighbours(step.GetTile(), types);
            //List<MapTile> neighbors = map.GetTileNeighbours(step.GetTile(), MapTile.TileType.OPEN);
            //neighbors.AddRange(map.GetTileNeighbours(step.GetTile(), MapTile.TileType.EXIT));

            List<MapTile> duplicates = new List<MapTile>();

            foreach (MapTile tile in neighbors)
            {
                if (closedSet.Any(pathStep => pathStep.GetTile() == tile) ||
                    fringe.Any(pathStep => pathStep.GetTile() == tile))
                {
                    duplicates.Add(tile);
                }
            }

            foreach (MapTile duplicate in duplicates)
            {
                neighbors.Remove(duplicate);
            }

            if (neighbors.Count > 0)
            {
                neighbors.Sort(
                delegate(MapTile x, MapTile y)
                {
                    return Game1.random.Next(3) - 1; // range -1 to 1, random result
                });

                foreach (MapTile mapTile in neighbors)
                {
                    fringe.Add(new PathStep(mapTile, creature.GetCurrentTile(), goalTiles, step, map));
                }
            }
        }

        private static bool PathStepIsGoal(PathStep pathStep)
        {
            return pathStep.IsGoal();
        }

        private static bool PathStepIsCloseToGoal(PathStep pathStep)
        {
            return (pathStep.GetTrueDistanceToGoal() <= CLOSE_TO_GOAL_VALUE);
        }

        private static bool PathStepIsWithinSearchRange(PathStep pathstep)
        {
            return pathstep.GetTotalValue() <= LOWEST_SCORE + SEARCH_RANGE;
        }
        /*
        public List<PathStep> GetClosedSet() {
            return this.closedSet;
        }*/


        public void DebugDraw(SpriteBatch spriteBatch, Camera camera) {
            foreach (PathStep step in closedSet) {
                Vector2 drawPos = new Vector2(
                         (this.map.GetPosition().X + this.map.GetTileSize().X * step.GetTile().GetCoordinateX()) - camera.GetPosition().X,
                    //+ map.GetTileSize().X / 2,  // center sprite
                    //+ map.GetTileSize().X * spriteOffset.X,    // offset
                         (this.map.GetPosition().Y + this.map.GetTileSize().Y * step.GetTile().GetCoordinateY()) - camera.GetPosition().Y);
                //+ map.GetTileSize().Y / 2);  // center sprite
                //+ map.GetTileSize().Y * spriteOffset.Y);   // offset

                spriteBatch.DrawString(Game1.graphLib.GetDefFont(),
                    step.GetTotalValue().ToString(),
                    drawPos,
                    Color.Orange);
            }
        }
    }
}
