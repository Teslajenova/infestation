﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Aids {
    class PerceivedTileFinder {

        private Map map;

        //private MapTile originTile;
        private List<MapTile> fringe;

        private List<MapTile.TileType> acceptableTypes;
        private int maxSteps;
        private int stepsTaken;

        private List<MapTile> perceivedTiles = new List<MapTile>();
        List<MapTile> newTiles = new List<MapTile>();

        public PerceivedTileFinder(Map map, MapTile originTile, List<MapTile.TileType> acceptableTypes, int maxSteps) {
            this.map = map;
            this.maxSteps = maxSteps;
            this.acceptableTypes = acceptableTypes;

            this.fringe = new List<MapTile>();
            fringe.Add(originTile);
            
        }

        public List<MapTile> GetCurrentlyPerceivedTiles() {
            return this.perceivedTiles;
        }

        public bool IsFinished() {
            if (fringe.Count == 0)
                return true;

            return (stepsTaken >= maxSteps);
        }

        public void Traverse() { // there are duplicates in the results! no there aren't
            if (fringe.Count > 0) {
                if (stepsTaken <= maxSteps) {
                    foreach (MapTile tile in fringe) {
                        List<MapTile> tempTiles = new List<MapTile>();

                        tempTiles.AddRange(map.GetTileNeighbours(tile));

                        foreach (MapTile tempTile in tempTiles) {
                            if (!newTiles.Contains(tempTile))
                                newTiles.Add(tempTile);
                        }
                    }

                    // shuffle
                    newTiles.Sort(
                        delegate(MapTile x, MapTile y) {
                            return Game1.random.Next(3) - 1; // range -1 to 1, random result
                        }
                    );

                    perceivedTiles.AddRange(fringe);
                    fringe = new List<MapTile>();


                    foreach (MapTile tile in newTiles) {
                        if (acceptableTypes.Contains(tile.GetTileType()))
                            fringe.Add(tile);
                    }

                    newTiles = new List<MapTile>();

                    stepsTaken++;
                }
            }
                /*
            else {
                stepsTaken = maxSteps + 1;
            }*/
        }
    }
}
