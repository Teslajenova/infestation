﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Ai.Aids
{
    class PathStep
    {

        static float START_DIST_VALUE_MODIFIER = 1f;
        static float GOAL_DIST_VALUE_MODIFIER = 2f;
        static float CROWD_VALUE_MODIFIER = 2.5f;

        MapTile myTile;

        float goalDistanceValue;
        float startDistanceValue;
        float crowdValue;

        PathStep previousStep;

        public PathStep(MapTile myTile, MapTile startTile, List<MapTile> goalTiles, PathStep previousStep, Map map)
        {
            this.myTile = myTile;
            this.previousStep = previousStep;

            // start value
            long startDistX = Math.Abs(myTile.GetCoordinateX() - startTile.GetCoordinateX());
            long startDistY = Math.Abs(myTile.GetCoordinateY() - startTile.GetCoordinateY());
            startDistanceValue = (startDistX + startDistY) * START_DIST_VALUE_MODIFIER;

            // crowd value
            crowdValue = 0f;
            if (map.TileIsOccupied(myTile))
            {
                List<MapTile> tileNeighbours = map.GetTileNeighbours(myTile);

                foreach (MapTile neighbour in tileNeighbours)
                {
                    if (map.TileIsOccupied(neighbour))
                    {
                        crowdValue += CROWD_VALUE_MODIFIER;
                    }
                }
            }

            // goal value
            goalDistanceValue = float.NaN;

            foreach (MapTile goalTile in goalTiles)
            {
                long goalDistX = Math.Abs(myTile.GetCoordinateX() - goalTile.GetCoordinateX());
                long goalDistY = Math.Abs(myTile.GetCoordinateY() - goalTile.GetCoordinateY());

                float tempGoalValue = (goalDistX + goalDistY) * GOAL_DIST_VALUE_MODIFIER;

                if (goalDistanceValue != goalDistanceValue || goalDistanceValue > tempGoalValue) // self comparison intentional (NaN)
                {
                    goalDistanceValue = tempGoalValue;
                }
            }

        }

        public float GetTotalValue()
        {
            return startDistanceValue + goalDistanceValue + crowdValue;
        }

        /*
        public float GetGoalDistanceValue()
        {
            return goalDistanceValue;
        }
         * */

        public PathStep GetPreviousStep()
        {
            return previousStep;
        }

        public bool IsGoal()
        {
            return (goalDistanceValue == 0);
        }

        public Double GetTrueDistanceToGoal()
        {
            return (goalDistanceValue / GOAL_DIST_VALUE_MODIFIER);
        }

        public MapTile GetTile()
        {
            return myTile;
        }
    }
}
