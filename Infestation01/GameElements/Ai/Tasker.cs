﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Creatures;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Maps;
using Infestation01.GameElements.Ai.Tasks;

namespace Infestation01.GameElements.Ai
{
    class Tasker
    {
        /*
         * basic tasks:
         * eat (stock food), sleep (nest), socialize, procreate, flight/fight (preserve life)
         * 
         * higher tasks/goals: (these are for derived classes)
         * gain status, gain allies, gain wealth, gain security, gain knowledge, gain renown
         * 
         * 
         * is this gonna cut it?
         * */

        /*
        enum State { 
            NORMAL,
            ACUTE,
            TERMINAL
        }*/

        SortedList<float, Task> priorities;
        

        //State hungerState = State.NORMAL; //???

        protected Creature hostCreature;
        protected Map map;

        public Tasker(Creature host, Map map)
        {
            this.hostCreature = host;
            this.map = map;
        }

        public void Update(GameTime gameTime)
        {
            HungerCheck(gameTime);
        
        }

        private void HungerCheck(GameTime gameTime)
        {
            hostCreature.IncreaseHunger(gameTime.ElapsedGameTime.Milliseconds * 0.001f);

            if (!hostCreature.IsOffMap()) {
                if (hostCreature.IsHungry(false)) {
                    hostCreature.AddTask(new Feed(hostCreature, map), false);
                }
                /*
                if (hostCreature.GetHunger() > hostCreature.GetHungerTolerance() * 1.5f)
                {
                    //hungerState = State.TERMINAL;
                }
                else if (hostCreature.GetHunger() > hostCreature.GetHungerTolerance())
                {
                    //hungerState = State.ACUTE;
                }*/
            }

            
            
        }
    }
}
