﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Items
{
    public abstract class Item
    {
        //protected long quantity;

        protected ItemHolder holder;

        protected bool edible;
        protected float nutrition;

        protected Texture2D texture;

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            if (this.texture != null)
            {
                spriteBatch.Draw(this.texture, position, Color.White);
            }
        }

        /*
        public long GetQuantity()
        {
            return this.quantity;
        }

        public void IncreaseQuantity(long qty)
        {
            this.quantity += qty;
        }*/

        public Texture2D GetTexture()
        {
            return this.texture;
        }

        public bool IsEdible()
        {
            return this.edible;
        }

        public static bool ItemIsEdible(Item item)
        {
            return item.IsEdible();
        }

        public float GetNutrition()
        {
            return nutrition;
        }

        public ItemHolder GetHolder() {
            return this.holder;
        }

        public void SetHolder(ItemHolder newHolder) {
            this.holder = newHolder;
        }

        /*
        public virtual String MyType()
        {
            return this.ToString();

            //return "Item";
        }*/
        /*
        public bool TypeEquals(String otherType)
        {
            return this.MyType().Equals(otherType);
        }*/
    }
}
