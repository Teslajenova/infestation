﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infestation01.GameElements.Items
{
    public class ItemHolder
    {

        private List<Item> allItems;

        private int lastItemCount = 0;

        public ItemHolder(List<Item> allItems)
        {
            this.allItems = allItems;
        }

        public List<Item> GetAll() {

            List<Item> allMyItems = allItems.FindAll(item => item.GetHolder() == this);

            lastItemCount = allMyItems.Count;

            return allMyItems;
            /*
            List<Item> allMyItems = new List<Item>();

            foreach (Item item in allItems) {
                if (item.GetHolder() == this)
                    allMyItems.Add(item);
            }

            return allMyItems;
             * */
        }

        public bool HasEdibles()
        {

            return GetAll().Any(item => item.IsEdible());

            /*
            foreach (Item item in GetAll())
            {
                if (items.Any(Item.ItemIsEdible))
                    return true;
                
                //if (item.IsEdible())
                  //  return true;
            }

            return false;*/
        }

        public List<Item> GetAllEdibles()
        {
            return GetAll().FindAll(item => item.IsEdible());
        }

        /*
        public Item TakeItem(String itemType, long quantity)
        {
            Item takenItem = null;

            Item matchingItem = items.Find(i => i.MyType().Equals(itemType));

            if (matchingItem != null)
            {
                if (matchingItem.GetQuantity() <= quantity)
                {
                    takenItem = matchingItem;
                    items.Remove(matchingItem);
                }
                else
            }

            return takenItem;
        }*/

        public void AddItem(Item newItem)
        {
            if (!allItems.Contains(newItem))
                allItems.Add(newItem);

            newItem.SetHolder(this);
        }

        public void AddItem(List<Item> newItems) {
            foreach (Item item in newItems) {
                AddItem(item);
            }
        }

        /*
        public void RemoveItem(Item removeItem)
        {
            items.Remove(removeItem);
        }*/

        public bool Contains(Item item)
        {
            return item.GetHolder() == this;
        }

        /*
        public void AddItem(Item newItem)
        {
            Item existingInstance = items.Find(i => i.MyType().Equals(newItem.MyType()));

            if (existingInstance != null)
            {
                existingInstance.IncreaseQuantity(newItem.GetQuantity());
            }
            else 
            {
                items.Add(newItem);
            }
        }
         * */
    }
}
