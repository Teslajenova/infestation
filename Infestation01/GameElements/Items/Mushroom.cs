﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Infestation01.Engine;

namespace Infestation01.GameElements.Items
{
    class Mushroom :Item
    {

        public Mushroom(GraphLib graphLib, ItemHolder holder)
        {
            
            this.texture = graphLib.GetShroom02();

            this.edible = true;
            this.nutrition = 20f;

            if (holder != null)
                holder.AddItem(this);
        }

        /*
        public override string MyType()
        {
            return "Mushroom";
        }*/
    }
}
