﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infestation01.GameElements.Attrs
{
    class Skills
    {
        public enum Skill
        {
            MINING,
            DARK_VISION, // ??
            ORIENTATION,
            EQUIP_TWO_HANDED_WPN, // DUAL_WIELD??
            RESILIENT,
            STRONG,
            DODGE_BLOW,
            SCALE_SHEER_SURFACE, // ??
            MIGHTY_BLOW,

            TEST_SKILL_1,
            TEST_SKILL_2,
            TEST_SKILL_3
        }

        List<Skill> possessed = new List<Skill>();

        public Skills(List<Skill> startingSkills)
        {
            foreach (Skill skill in startingSkills)
            {
                this.possessed.Add(skill);
            }
        }

        public void Add(Skill newSkill)
        {
            if(!possessed.Contains(newSkill))
            {
                possessed.Add(newSkill);
            }
        }
    }
}
