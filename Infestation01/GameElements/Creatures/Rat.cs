﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.Engine;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Creatures
{
    class Rat : Creature
    {

        public Rat(MapTile currentTile, Map map, GraphLib graphLib)
        {
            this.texture = graphLib.GetRat02();

            this.map = map;
            this.currentTile = currentTile;

            this.drawScale = 0.6f;
        }
    }
}
