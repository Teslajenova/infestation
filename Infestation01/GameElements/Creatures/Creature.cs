﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Infestation01.Engine;
using Infestation01.GameElements.Ai;
using Infestation01.GameElements.Ai.Tasks;
using Infestation01.GameElements.Items;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Creatures
{
     abstract class Creature
    {
         public enum State
         {
             IDLE,
             WALKING,
             WAITING,
             EATING,
             CROUCHING,
             THINKING
         }

         State currentState = State.IDLE; 

         // traits
         /*
         public enum Trait
         {
             SPEED,
             PATIENCE
         }

         protected SortedList<Trait, int> traits = new SortedList<Trait, int>();
         */
         protected int speed;
         protected int patience;
         protected int perception;

         protected float damage;
         protected float damageTolerance;

         protected float hunger;
         protected float hungerTolerance;
         protected float gluttony;

         protected float fatique;
         protected float fatiqueTolerance;

         protected bool offMap = false;

         protected MapTile currentTile;
         protected MapTile waitingToEnter;

         //protected List<Item> items = new List<Item>();
         protected ItemHolder items;

         protected Map map;

         protected Texture2D texture;
         protected float drawScale;

         protected Tasker tasker;
         protected List<Task> taskBank = new List<Task>();
         protected Task currentTask;

         protected Vector2 spriteOffset = new Vector2();
         //protected float spriteAlpha = 0f;

         public void Update(GameTime gameTime)
         {
             this.tasker.Update(gameTime);
             this.RunTasks(gameTime);
         }

         private void RunTasks(GameTime gameTime)
         {
             if (taskBank.Count > 0)
             {
                 if (currentTask == null || currentTask.IsDone() || currentTask.UnableToFinish())
                 {
                     currentTask = taskBank.ElementAt(0);
                     taskBank.Remove(currentTask);
                 }
             }

             if (currentTask != null)
             {
                 currentTask.Update(gameTime);

                 if (currentTask.IsDone() || currentTask.UnableToFinish())
                     currentTask = null;
             }
         }

         public void AddTask(Task newTask, Boolean allowDuplicate)
         {
             if (!allowDuplicate)
             {
                 if ((currentTask != null && currentTask.TaskType().Equals(newTask.TaskType())) || 
                     taskBank.Any(task => task.TaskType().Equals(newTask.TaskType())))
                     return;
             }

             this.taskBank.Add(newTask);
         }

         public void SetSpriteOffset(Vector2 offset)
         {
             spriteOffset = offset;
         }

         public void Draw(SpriteBatch spriteBatch, Camera camera)
         {
             if (!offMap && this.texture != null && this.map != null && this.currentTile != null)
             {
                 Vector2 drawPos = new Vector2(
                     (this.map.GetPosition().X + this.map.GetTileSize().X * this.currentTile.GetCoordinateX()) - camera.GetPosition().X
                     + map.GetTileSize().X / 2  // center sprite
                     + map.GetTileSize().X * spriteOffset.X,    // offset
                     (this.map.GetPosition().Y + this.map.GetTileSize().Y * this.currentTile.GetCoordinateY()) - camera.GetPosition().Y
                     + map.GetTileSize().Y / 2  // center sprite
                     + map.GetTileSize().Y * spriteOffset.Y);   // offset

                 //spriteBatch.Draw(this.texture, drawPos, null, Color.White, 0f, new Vector2(), this.drawScale, SpriteEffects.None, 0f);

                 Vector2 spriteDimensions = new Vector2(this.texture.Width, this.texture.Height);

                 Color color = Color.White;
                 //Vector4 colorVector = color.ToVector4();
                 //colorVector.W = spriteAlpha;
                 //color = new Color(colorVector);

                 spriteBatch.Draw(
                     this.texture, 
                     drawPos, 
                     null, 
                     color, 
                     0f, 
                     new Vector2(spriteDimensions.X / 2, spriteDimensions.Y / 2), 
                     this.drawScale, 
                     SpriteEffects.None, 
                     0f);


                 Color stateTextColor = Color.Yellow;
                 if (currentState == State.THINKING)
                 {
                     stateTextColor = Color.MediumBlue;
                 }
                 else if (currentState == State.IDLE) {
                     stateTextColor = Color.LightGreen;
                 }

                 spriteBatch.DrawString(
                     Game1.graphLib.GetDefFont(),
                     this.currentState.ToString() + ":" + Math.Floor(this.hunger),
                     drawPos,
                     stateTextColor,
                     0f,
                     new Vector2(0),
                     0.6f,
                     SpriteEffects.None,
                     0f);
             }

             // debug
             /*if (currentTask != null)
                currentTask.DebugDraw(spriteBatch, camera);
              * */
         }

         
         public int GetSpeed()
         {
             return speed;
         }

         public int GetPatience()
         {
             return patience;
         }

         public int GetPerception() {
             return this.perception;
         }

         public bool IsHungry(bool countGluttony)
         {
             float totalHunger = hunger;

             if (countGluttony)
                 totalHunger += gluttony;

             if (totalHunger > hungerTolerance)
             {
                 return true;
             }

             return false;
         }

         public void ConsumeItem(Item item)
         {
             hunger -= item.GetNutrition();
             item.SetHolder(null);
         }

         /*public float GetHunger()
         {
             return hunger;
         }*/

         public void IncreaseHunger(float increase)
         {
             hunger += increase;
         }

         public float GetHungerTolerance()
         {
             return hungerTolerance;
         }

         
         public ItemHolder GetItems()
         {
             return this.items;
         }
         

         public MapTile GetCurrentTile()
         {
             return currentTile;
         }

         public void SetCurrentTile(MapTile newTile)
         {
             this.currentTile = newTile;
             this.waitingToEnter = null;
         }

         public MapTile GetWaitingToEnter()
         {
             return waitingToEnter;
         }

         public void SetWaitingToEnter(MapTile newTile)
         {
             this.waitingToEnter = newTile;
         }

         public bool IsOffMap()
         {
             return this.offMap;
         }

         public void SetOffMap(bool offMap)
         {
             this.offMap = offMap;
         }
         /*
         protected void SetSpeed(int speed)
         {
             this.speed = speed;
         }
          * */

         public void SetCurrentState(State state)
         {
             this.currentState = state;
         }

         public State GetCurrentState() {
             return this.currentState;
         }

         public bool IsIdle() {
            return (this.currentTask == null && this.taskBank.Count == 0 && this.currentState == State.IDLE);
                
         }

         public String CreatureType() {
             return this.ToString();
         }
    }
}
