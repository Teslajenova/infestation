﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.Engine;
using Infestation01.GameElements.Ai;
using Infestation01.GameElements.Attrs;
using Infestation01.GameElements.Jobs;
using Infestation01.GameElements.Items;
using Infestation01.GameElements.Maps;

namespace Infestation01.GameElements.Creatures
{
    class Moogle : Creature
    {

        protected Skills skills;

        protected Job job;

        public Moogle(MapTile currentTile, Map map, GraphLib graphLib, List<Item> allItems)
        {
            this.texture = graphLib.GetMog();
            this.items = new ItemHolder(allItems);

            this.map = map;
            this.currentTile = currentTile;

            this.drawScale = 1.3f;

            Random random = Game1.random;

            //this.InitBasicTraits();
            //this.traits.

            speed = random.Next(50) + 30;
            patience = random.Next(30) + 30;
            perception = 8 + random.Next(10);

            hungerTolerance = random.Next(50) + 50;
            hunger = random.Next(40) + 30;

            // set job
            this.job = new Warrior();

            // set skills
            List<Skills.Skill> startingSkills = new List<Skills.Skill>();
            startingSkills.Add(Skills.Skill.TEST_SKILL_1);
            this.skills = new Skills(startingSkills);
            this.skills.Add(Skills.Skill.TEST_SKILL_2);

            this.tasker = new Tasker(this, map);

            // add items
            /*for (int i = 0; i < random.Next(6) - 3; i++)
            {
                this.items.AddItem(new Mushroom(graphLib, this.items));
            }*/
            
        }
    }
}
