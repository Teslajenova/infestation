﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Attrs;

namespace Infestation01.GameElements.Jobs
{
    abstract class Job
    {

        protected List<Job> careerOptions;
        protected List<Skills.Skill> jobSkills;

        protected void SetJobSkills(List<Skills.Skill> skills)
        {
            this.jobSkills = skills;
        }
    }
}
