﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Attrs;

namespace Infestation01.GameElements.Jobs
{
    class Warrior : Job
    {

        public Warrior()
        {

            // skills
            List<Skills.Skill> skills = new List<Skills.Skill>();
            skills.Add(Skills.Skill.DODGE_BLOW);
            skills.Add(Skills.Skill.SCALE_SHEER_SURFACE);
            skills.Add(Skills.Skill.MIGHTY_BLOW);

            SetJobSkills(skills);
        }
    }
}
