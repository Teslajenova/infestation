﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Attrs;

namespace Infestation01.GameElements.Jobs
{
    class Miner : Job
    {

        public Miner()
        {

            // skills
            List<Skills.Skill> skills = new List<Skills.Skill>();
            skills.Add(Skills.Skill.TEST_SKILL_3);
            skills.Add(Skills.Skill.TEST_SKILL_2);

            SetJobSkills(skills);
        }
    }
}
