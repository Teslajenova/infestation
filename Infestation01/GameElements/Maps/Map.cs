﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Infestation01.Engine;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Items;

namespace Infestation01.GameElements.Maps
{
    class Map
    {
        public enum Direction { NONE, UP, DOWN, LEFT, RIGHT };

        Random random;
        GraphLib graphLib;

        Vector2 position;

        List<MapTile> tiles;

        MapTile originTile;

        Vector2 tileSize;

        Texture2D tileTextureOpen;
        Texture2D tileTextureHole;

        SpriteFont debugFont;

        MapTileOccupancyResolver tileOccupancyResolver;

        private List<Item> allItems;

        // constructor
        public Map(Vector2 tileSize, Vector2 position, int initialTiles, GraphLib graphLib, List<Creature> creatures, List<Item> allItems)
        {
            this.random = new Random();
            this.graphLib = graphLib;
            this.allItems = allItems;
            this.position = position;

            this.tileTextureOpen = graphLib.GetDirt();
            this.tileTextureHole = graphLib.GetHole();
            this.debugFont = graphLib.GetDefFont();

            this.tileSize = tileSize;

            this.tiles = new List<MapTile>();

            this.originTile = new MapTile(0, 0, this, this.random, graphLib, allItems, MapTile.TileType.EXIT);

            this.tiles.Add(this.originTile);

            this.tiles.Add(new MapTile(0, 1, this, this.random, graphLib, allItems, MapTile.TileType.SOLID));
            this.tiles.Add(new MapTile(0, -1, this, this.random, graphLib, allItems, MapTile.TileType.SOLID));
            this.tiles.Add(new MapTile(1, 0, this, this.random, graphLib, allItems, MapTile.TileType.SOLID));
            this.tiles.Add(new MapTile(-1, 0, this, this.random, graphLib, allItems, MapTile.TileType.SOLID));


            this.OpenTile(this.tiles[1]);
            this.OpenRandomTiles(initialTiles);


            this.tileOccupancyResolver = new MapTileOccupancyResolver(this, creatures); 
        }


        public void OpenRandomTiles(int numberOfTiles)
        {
            int counter = numberOfTiles;

            while (counter > 0)
            {
                int rndIndex = this.random.Next(this.tiles.Count);

                if (this.tiles[rndIndex].GetTileType() == MapTile.TileType.SOLID && this.IsNextToOpenTile(this.tiles[rndIndex],1))
                {
                    this.OpenTile(this.tiles[rndIndex]);
                    counter--;
                }
            }

        }

        public bool IsNextToOpenTile(MapTile mapTile)
        {
            bool nextToOpenTile = false;

            List<MapTile> neighbours = this.GetTileNeighbours(mapTile);

            foreach (MapTile mt in neighbours)
            {
                if (mt.GetTileType() == MapTile.TileType.OPEN)
                    nextToOpenTile = true;
            }

            return nextToOpenTile;
        }

        public bool IsNextToOpenTile(MapTile mapTile, int numberOfAdjacentOpenTiles)
        {
            bool nextToOpenTile = false;
            int counter = 0;

            List<MapTile> neighbours = this.GetTileNeighbours(mapTile);

            foreach (MapTile mt in neighbours)
            {
                if (mt.GetTileType() == MapTile.TileType.OPEN)
                    counter++;
            }

            if (counter == numberOfAdjacentOpenTiles)
            {
                nextToOpenTile = true;
            }

            return nextToOpenTile;
        }

        public void OpenTile(long coordinateX, long coordinateY)
        {
            MapTile tile = this.GetTileByCoordinates(coordinateX, coordinateY);
            this.OpenTile(tile);
        }

        public void OpenTile(MapTile tileToOpen)
        {
            if (tileToOpen != null && tileToOpen.GetTileType() != MapTile.TileType.OPEN && tileToOpen.GetTileType() != MapTile.TileType.EXIT)
            {
                tileToOpen.SetTileType(MapTile.TileType.OPEN);

                List<MapTile> neighbours = this.GetTileNeighbours(tileToOpen);

                bool upTileExists = false;
                bool downTileExists = false;
                bool leftTileExists = false;
                bool rightTileExists = false;

                foreach (MapTile mt in neighbours)
                {
                    if (mt.GetCoordinateX() == tileToOpen.GetCoordinateX() && mt.GetCoordinateY() == tileToOpen.GetCoordinateY() - 1)
                        upTileExists = true;
                    else if (mt.GetCoordinateX() == tileToOpen.GetCoordinateX() && mt.GetCoordinateY() == tileToOpen.GetCoordinateY() + 1)
                        downTileExists = true;
                    else if (mt.GetCoordinateX() == tileToOpen.GetCoordinateX() - 1 && mt.GetCoordinateY() == tileToOpen.GetCoordinateY())
                        leftTileExists = true;
                    else if (mt.GetCoordinateX() == tileToOpen.GetCoordinateX() + 1 && mt.GetCoordinateY() == tileToOpen.GetCoordinateY())
                        rightTileExists = true;

                }

                if (!upTileExists)
                    this.tiles.Add(new MapTile(
                        tileToOpen.GetCoordinateX(), tileToOpen.GetCoordinateY() - 1,
                        this, this.random, this.graphLib,
                        //this.GetTileNeighbours(tileToOpen.GetCoordinateX(), tileToOpen.GetCoordinateY() - 1),
                        allItems,
                        MapTile.TileType.SOLID
                        ));

                if (!downTileExists)
                    this.tiles.Add(new MapTile(
                        tileToOpen.GetCoordinateX(), tileToOpen.GetCoordinateY() + 1,
                        this, this.random, this.graphLib,
                        //this.GetTileNeighbours(tileToOpen.GetCoordinateX(), tileToOpen.GetCoordinateY() + 1),
                        allItems,
                        MapTile.TileType.SOLID
                        ));

                if (!leftTileExists)
                    this.tiles.Add(new MapTile(
                        tileToOpen.GetCoordinateX() - 1, tileToOpen.GetCoordinateY(),
                        this, this.random, this.graphLib,
                        //this.GetTileNeighbours(tileToOpen.GetCoordinateX() - 1, tileToOpen.GetCoordinateY()),
                        allItems,
                        MapTile.TileType.SOLID
                        ));

                if (!rightTileExists)
                    this.tiles.Add(new MapTile(
                        tileToOpen.GetCoordinateX() + 1, tileToOpen.GetCoordinateY(),
                        this, this.random, this.graphLib,
                        //this.GetTileNeighbours(tileToOpen.GetCoordinateX() + 1, tileToOpen.GetCoordinateY()),
                        allItems,
                        MapTile.TileType.SOLID
                        ));


            }
        }

        public MapTile GetAdjacentTileIfCorrectType(MapTile originTile, Direction direction, List<MapTile.TileType> types)
        {
            MapTile targetTile = GetAdjacentTile(originTile, direction);

            for (int i = 0; i < types.Count; i++)
            {
                if (targetTile.GetTileType() == types.ElementAt(i))
                {
                    return targetTile;
                }
            }

            return null;
        }

        public MapTile GetAdjacentTileIfCorrectType(MapTile originTile, Direction direction, MapTile.TileType type)
        {
            MapTile targetTile = GetAdjacentTile(originTile, direction);

            if (targetTile.GetTileType() != type)
                return null;

            return targetTile;
        }

        public MapTile GetAdjacentTile(MapTile originTile, Direction direction)
        {
            MapTile targetTile = null;

            if (direction == Direction.UP && GetTileByCoordinates(originTile.GetCoordinateX(), originTile.GetCoordinateY() - 1) != null)
            {
                targetTile = GetTileByCoordinates(originTile.GetCoordinateX(), originTile.GetCoordinateY() - 1);    
            }
            else if (direction == Direction.DOWN && GetTileByCoordinates(originTile.GetCoordinateX(), originTile.GetCoordinateY() + 1) != null)
            {
                targetTile = GetTileByCoordinates(originTile.GetCoordinateX(), originTile.GetCoordinateY() + 1);
            }
            else if (direction == Direction.LEFT && GetTileByCoordinates(originTile.GetCoordinateX() - 1, originTile.GetCoordinateY()) != null)
            {
                targetTile = GetTileByCoordinates(originTile.GetCoordinateX() - 1, originTile.GetCoordinateY());
            }
            else if (direction == Direction.RIGHT && GetTileByCoordinates(originTile.GetCoordinateX() + 1, originTile.GetCoordinateY()) != null)
            {
                targetTile = GetTileByCoordinates(originTile.GetCoordinateX() + 1, originTile.GetCoordinateY());
            }

            return targetTile;
        }

        
        public Boolean TileIsOccupied(MapTile tile)
        {
            return tileOccupancyResolver.TileIsOccupied(tile);
        }

        public Creature GetOccupyingCreature(MapTile tile)
        {
            return tileOccupancyResolver.GetOccupyingCreature(tile);
        }



        public List<MapTile> GetTileNeighbours(long coordinateX, long coordinateY, List<MapTile.TileType> types) {
            List<MapTile> neighbours = new List<MapTile>();

            if (this.GetTileByCoordinates(coordinateX, coordinateY - 1) != null) {
                if (types.Contains(MapTile.TileType.NONE) || types.Contains(this.GetTileByCoordinates(coordinateX, coordinateY - 1).GetTileType())) {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX, coordinateY - 1));
                }

            }

            if (this.GetTileByCoordinates(coordinateX, coordinateY + 1) != null) {
                if (types.Contains(MapTile.TileType.NONE) || types.Contains(this.GetTileByCoordinates(coordinateX, coordinateY + 1).GetTileType())) {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX, coordinateY + 1));
                }
            }

            if (this.GetTileByCoordinates(coordinateX - 1, coordinateY) != null) {
                if (types.Contains(MapTile.TileType.NONE) || types.Contains(this.GetTileByCoordinates(coordinateX - 1, coordinateY).GetTileType())) {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX - 1, coordinateY));
                }
            }

            if (this.GetTileByCoordinates(coordinateX + 1, coordinateY) != null) {
                if (types.Contains(MapTile.TileType.NONE) || types.Contains(this.GetTileByCoordinates(coordinateX + 1, coordinateY).GetTileType())) {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX + 1, coordinateY));
                }
            }

            return neighbours;
        } 

        
        public List<MapTile> GetTileNeighbours(long coordinateX, long coordinateY, MapTile.TileType type)
        {
            List<MapTile> neighbours = new List<MapTile>();

            if (this.GetTileByCoordinates(coordinateX, coordinateY - 1) != null)
            {
                if (type == MapTile.TileType.NONE || this.GetTileByCoordinates(coordinateX, coordinateY - 1).GetTileType() == type)
                {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX, coordinateY - 1));
                }
                
            }

            if (this.GetTileByCoordinates(coordinateX, coordinateY + 1) != null)
            {
                if (type == MapTile.TileType.NONE || this.GetTileByCoordinates(coordinateX, coordinateY + 1).GetTileType() == type)
                {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX, coordinateY + 1));
                }
            }

            if (this.GetTileByCoordinates(coordinateX - 1, coordinateY) != null)
            {
                if (type == MapTile.TileType.NONE || this.GetTileByCoordinates(coordinateX - 1, coordinateY).GetTileType() == type)
                {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX - 1, coordinateY));
                }
            }

            if (this.GetTileByCoordinates(coordinateX + 1, coordinateY) != null)
            {
                if (type == MapTile.TileType.NONE || this.GetTileByCoordinates(coordinateX + 1, coordinateY).GetTileType() == type)
                {
                    neighbours.Add(this.GetTileByCoordinates(coordinateX + 1, coordinateY));
                }
            }                

            return neighbours;
        }
        

        public List<MapTile> GetTileNeighbours(long coordinateX, long coordinateY)
        {
            return this.GetTileNeighbours(coordinateX, coordinateY, MapTile.TileType.NONE);
        }

        public List<MapTile> GetTileNeighbours(MapTile centerTile)
        {
            return this.GetTileNeighbours(centerTile.GetCoordinateX(), centerTile.GetCoordinateY());
        }

        public List<MapTile> GetTileNeighbours(MapTile centerTile, List<MapTile.TileType> types) {
            return this.GetTileNeighbours(centerTile.GetCoordinateX(), centerTile.GetCoordinateY(), types);
        }

        public List<MapTile> GetTileNeighbours(MapTile centerTile, MapTile.TileType type) {
            return this.GetTileNeighbours(centerTile.GetCoordinateX(), centerTile.GetCoordinateY(), type);
        }

        public MapTile GetTileByCoordinates(long coordinateX, long coordinateY)
        {
            MapTile tileAtCoordinates = null;

            foreach (MapTile tile in this.tiles)
            {
                if (tile.GetCoordinateX() == coordinateX && tile.GetCoordinateY() == coordinateY)
                    tileAtCoordinates = tile;
            }

            return tileAtCoordinates;
        }

        public MapTile GetRandomTile(MapTile.TileType tileType)
        {
            MapTile tile = null;

            while (tile == null)
            {
                int random = this.random.Next(this.tiles.Count);

                if (tileType == MapTile.TileType.NONE)
                {
                    tile = this.tiles[random];
                }
                else if (this.tiles[random].GetTileType() == tileType)
                {
                    tile = this.tiles[random];
                }
            }

            return tile;
        }

        public void Update(GameTime gameTime)
        {
            foreach (MapTile tile in this.tiles)
            {
                tile.Update(gameTime);
            }
        }

        public List<MapTile> GetNearbyTilesWithFood(Creature creature) {
            List<MapTile> foodTiles = new List<MapTile>();

            List<MapTile.TileType> types = new List<MapTile.TileType>();
            types.Add(MapTile.TileType.OPEN);
            types.Add(MapTile.TileType.EXIT);
            List<MapTile> perceivedTiles = GetPerceivedTiles(creature, types);

            foreach (MapTile tile in perceivedTiles) {
                if (tile.GetItems().HasEdibles())
                    foodTiles.Add(tile);
            }

            return foodTiles;
        }

        public List<MapTile> GetPerceivedTiles(Creature creature, List<MapTile.TileType> acceptableTypes) {
            List<MapTile> perceivedTiles = new List<MapTile>();

            //MapTile originTile = creature.GetCurrentTile();

            List<MapTile> fringe = new List<MapTile>();
            fringe.Add(creature.GetCurrentTile());

            List<MapTile> newTiles = new List<MapTile>();

            for (int i = 0; i < creature.GetPerception(); i++) {
                if (fringe.Count > 0) {
                    foreach (MapTile tile in fringe) {
                        List<MapTile> tempTiles = new List<MapTile>();

                        tempTiles.AddRange(GetTileNeighbours(tile));

                        foreach (MapTile tempTile in tempTiles) {
                            if (!newTiles.Contains(tempTile))
                                newTiles.Add(tempTile);
                        }
                    }

                    perceivedTiles.AddRange(fringe);
                    fringe = new List<MapTile>();

                    
                    foreach (MapTile tile in newTiles) {
                        if (acceptableTypes.Contains(tile.GetTileType()))
                            fringe.Add(tile);
                    }

                    newTiles = new List<MapTile>();
                }
            }

            return perceivedTiles;
        }

        public List<MapTile> GetAllTilesOfType(MapTile.TileType type)
        {
            List<MapTile> typeTiles = new List<MapTile>();

            foreach (MapTile tile in tiles)
            {
                if (tile.GetTileType() == type)
                {
                    typeTiles.Add(tile);
                }
            }

            return typeTiles;
        }

        public void Draw(SpriteBatch spriteBatch, Camera camera)
        {
            //spriteBatch.Draw(this.tileTexture, this.position, Color.White);
            foreach(MapTile tile in this.tiles) //(int aa = 0; aa < this.tiles.Count; aa++)
            {
                Texture2D texture = this.tileTextureOpen;
                Color color = Color.White;

                if (tile.GetTileType() == MapTile.TileType.EXIT)
                {
                    texture = this.tileTextureHole;
                }
                else if (tile.GetTileType() == MapTile.TileType.SOLID)
                {
                    color = Color.DarkGray;
                }

                Vector2 tileDrawSize = new Vector2(this.tileSize.X / texture.Width, this.tileSize.Y / texture.Height);

                Vector2 tileDrawPos = new Vector2(
                        (this.position.X + this.tileSize.X * tile.GetCoordinateX()) - camera.GetPosition().X,
                        (this.position.Y + this.tileSize.Y * tile.GetCoordinateY()) - camera.GetPosition().Y);

                spriteBatch.Draw(
                    texture,
                    tileDrawPos,
                    null, color, 0f, new Vector2(0f, 0f), tileDrawSize, SpriteEffects.None, 0f);

                tile.DrawItems(spriteBatch, tileDrawPos,this.debugFont);

                String infoString = null;

                //if (tile.GetFertility() > 0)
                  //  infoString = tile.GetFertility().ToString();

                //if (tile.GetMushroom() != null)
                  //  infoString = infoString + ":" + tile.GetMushroom().GetQuantity().ToString();

                if (infoString != null)
                {
                    spriteBatch.DrawString(
                    this.debugFont,
                    infoString,
                    new Vector2(tileDrawPos.X, tileDrawPos.Y + 15),
                    Color.White, 0f, new Vector2(0f), 0.7f,
                    SpriteEffects.None, 0f);
                }
                
                 
            }
        }

        public Vector2 GetPosition()
        {
            return this.position;
        }

        public Vector2 GetTileSize()
        {
            return this.tileSize;
        }
    }
}
