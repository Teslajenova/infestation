﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Infestation01.GameElements.Items;
using Infestation01.Engine;
using Microsoft.Xna.Framework.Graphics;

namespace Infestation01.GameElements.Maps
{

    class MapTile
    {

        Random random;
        GraphLib graphLib;
        Map map;

        public enum TileType{OPEN, SOLID, EXIT, NONE};

        float fertility;
        float fertilityCounter;

        long coordinateX;
        long coordinateY;

        TileType tileType;

        //List<Item> items;
        private ItemHolder items;

        //private List<Mushroom> mushrooms = new List<Mushroom>(); // <-- FIX THAT : it doesn't get reduced when picked up --
        //Mushroom mushrooms; // just another item?

        // constructor
        /*public MapTile(long coordinateX, long coordinateY, Map map, Random random, GraphLib graphLib, List<MapTile> neighbors, List<Item> allItems)
        {
            this.CommonConstructor(coordinateX, coordinateY, map, random, graphLib);
            this.tileType = TileType.SOLID;
            this.fertility = this.SetFertility(neighbors);
            this.ResetFertilityCounter();
            this.items = new ItemHolder(allItems);
        }*/

        public MapTile(long coordinateX, long coordinateY, Map map, Random random, GraphLib graphLib, List<Item> allItems, TileType tileType)
        {
            this.CommonConstructor(coordinateX, coordinateY, map, random, graphLib);
            this.tileType = tileType;
            this.fertility = this.SetFertility(map.GetTileNeighbours(coordinateX, coordinateY)); //this.SetFertility(neighbors);
            this.ResetFertilityCounter();
            this.items = new ItemHolder(allItems);
        }

        private void CommonConstructor(long coordinateX, long coordinateY, Map map, Random random, GraphLib graphLib)
        {
            this.random = random;
            this.graphLib = graphLib;
            this.map = map;
            this.coordinateX = coordinateX;
            this.coordinateY = coordinateY;

            this.fertilityCounter = 0f;

            //this.items = new List<Item>();
        }

        private float SetFertility(List<MapTile> neighbors)
        {
            float fert = 0f;

            if (this.tileType != TileType.EXIT)
            {
                float maxFert = 0f;

                foreach(MapTile neighbor in neighbors)
                {
                    maxFert += neighbor.GetFertility() * 0.9f;
                }

                fert = this.random.Next((int)(maxFert));

                bool bonus = false;
                if (this.random.Next(30) == 0)
                    bonus = true;

                if (bonus)
                    fert += this.random.Next(10);
            }

            return fert;
        }

        private void GrowMushrooms(GameTime gameTime)
        {
            if (this.tileType == TileType.OPEN && this.fertility > 0)
            {

                long mushroomSpace = (long)(this.fertility);
                mushroomSpace -= this.GetMushroomCount(); //this.mushrooms.GetQuantity();

                if (mushroomSpace > 0)
                {   
                    this.fertilityCounter -= gameTime.ElapsedGameTime.Milliseconds * mushroomSpace;

                    if(this.fertilityCounter < 0)
                    {
                        this.AddMushroom(1);

                        this.ResetFertilityCounter();
                    }

                    
                }

            }
        }

        private void ResetFertilityCounter()
        {
            long counterMax = 7000L - (long)(this.fertility);
            foreach (MapTile neighbour in this.map.GetTileNeighbours(this))
            {
                //if (neighbour.GetMushroom() != null)
                    counterMax -= neighbour.GetMushroomCount(); //neighbour.GetMushroom().GetQuantity();
            }

            this.fertilityCounter = counterMax;
        }

        private void AddMushroom(int qty)
        {
            for (int i = 0; i < qty; i++)
            {
                Mushroom m = new Mushroom(this.graphLib, this.items);
                items.AddItem(m);
                //mushrooms.Add(m);
            }


            /*
            if (this.mushrooms == null)
            {
                this.mushrooms = new Mushroom(qty, this.graphLib);
                this.items.AddItem(this.mushrooms);
                //this.items.Add(this.mushrooms);
            }
            else
            {
                this.mushrooms.IncreaseQuantity(qty);
            }
            /*
            Mushroom mushroom = null;
            foreach (Item item in this.items)
            {
                if (item is Mushroom)
                {
                    mushroom = (Mushroom)(item);
                    break;
                }
            }

            if (mushroom == null)
            {
                this.items.Add(new Mushroom(qty, this.graphLib));
            }
            else 
            {
                mushroom.IncreaseQuantity(qty);
            }*/

            
        }

        public int GetMushroomCount()
        {
            return GetMushrooms().Count;
        }

        public List<Item> GetMushrooms(){
            Mushroom exampleMushroom = new Mushroom(graphLib,null);

            List<Item> mushrooms = this.items.GetAll().FindAll(item => item.ToString().Equals(exampleMushroom.ToString()));

            return mushrooms;
            //return this.mushrooms.Count;
            //return this.mushrooms;
        }

        public long GetCoordinateX()
        {
            return this.coordinateX;
        }

        public long GetCoordinateY()
        {
            return this.coordinateY;
        }

        public ItemHolder GetItems() {
            return this.items;
        }

        public TileType GetTileType()
        {
            return this.tileType;
        }

        public void SetTileType(TileType tileType)
        {
            this.tileType = tileType;
        }

        public float GetFertility()
        {
            return this.fertility;
        }

        public void Update(GameTime gameTime)
        {
            if (this.fertility > 0 && this.tileType == TileType.OPEN)
            {
                this.GrowMushrooms(gameTime);
            }
        }

        public void DrawItems(SpriteBatch spriteBatch, Vector2 drawPosition, SpriteFont font)
        {
            //if (this.items.Count > 0)
            //    spriteBatch.DrawString(font, "Items", drawPosition, Color.Yellow, 0f, new Vector2(0f), 0.6f, SpriteEffects.None, 0f);

            if (GetMushroomCount() > 0) //(mushrooms != null)
            {
                spriteBatch.Draw(
                    this.GetMushrooms().First().GetTexture(), //mushrooms.GetTexture(), 
                    drawPosition, 
                    Color.White);
                spriteBatch.DrawString(
                    font, 
                    "  " + this.GetMushroomCount().ToString(), //mushrooms.GetQuantity().ToString(), 
                    drawPosition,
                    Color.Yellow, 
                    0f, 
                    new Vector2(0f), 
                    0.7f, 
                    SpriteEffects.None, 
                    0f);
            }

            /*
            foreach(Item item in this.items)
            {
                if (item is Mushroom)
                {
                    spriteBatch.Draw(item.GetTexture(), drawPosition, Color.White);
                    spriteBatch.DrawString(font, "  " + item.GetQuantity().ToString(), drawPosition, 
                        Color.Yellow, 0f, new Vector2(0f), 0.7f, SpriteEffects.None, 0f);
                }
            }
             * */
        }
    }
}
