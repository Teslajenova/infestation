﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace Infestation01.Engine
{
    class Camera
    {
        public enum CameraMoveDirection { UP, DOWN, LEFT, RIGHT };

        Vector2 position;

        float panSpeed;

        public Camera(Vector2 position, float panSpeed)
        {
            this.position = position;
            this.panSpeed = panSpeed;
        }

        public void Update(GameTime gameTime)
        { }


        public Vector2 GetPosition()
        {
            return this.position;
        }

        public void MoveCamera(CameraMoveDirection moveDirection)
        {
            if (moveDirection == CameraMoveDirection.UP)
                this.position = new Vector2(this.position.X, this.position.Y - this.panSpeed);
            else if (moveDirection == CameraMoveDirection.DOWN)
                this.position = new Vector2(this.position.X, this.position.Y + this.panSpeed);
            else if (moveDirection == CameraMoveDirection.LEFT)
                this.position = new Vector2(this.position.X - this.panSpeed, this.position.Y);
            else if (moveDirection == CameraMoveDirection.RIGHT)
                this.position = new Vector2(this.position.X + this.panSpeed, this.position.Y);
        }

    }
}
