﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Infestation01.GameElements.Maps;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Ai.Tasks;

namespace Infestation01.Engine
{
    class Input
    {

        KeyboardState keyboardStateCurrent;
        KeyboardState keyboardStatePrevious;

        Texture2D mouseTexture;
        Vector2 mousePosition;

        Stage stage;
        Camera camera;
        Map map;

        Creature controlledCreature;
        private List<Creature> allCreatures;

        

        public long debugCX;
        public long debugCY;

        public Input(Stage stage, Camera camera, Map map, GraphLib graphLib)
        {
            this.stage = stage;
            this.camera = camera;
            this.map = map;

            //this.allCreatures = allCreatures;

            this.mouseTexture = graphLib.GetCross();

            this.keyboardStatePrevious = Keyboard.GetState();
        }

        public void Update()
        {
            // get new kb state
            this.keyboardStateCurrent = Keyboard.GetState();

            // get mouse position
            this.mousePosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            if (this.KeyDown(Keys.Up))
                this.camera.MoveCamera(Camera.CameraMoveDirection.UP);
            if (this.KeyDown(Keys.Down))
                this.camera.MoveCamera(Camera.CameraMoveDirection.DOWN);
            if (this.KeyDown(Keys.Left))
                this.camera.MoveCamera(Camera.CameraMoveDirection.LEFT);
            if (this.KeyDown(Keys.Right))
                this.camera.MoveCamera(Camera.CameraMoveDirection.RIGHT);

            if (NewlyPressedKey(Keys.N))
                this.stage.GetMap().OpenRandomTiles(1);

            if (this.NewlyPressedKey(Keys.M) || Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                this.map.OpenTile(this.GetMapTileUnderCursor());
            }

            if (this.NewlyPressedKey(Keys.P)) {
                foreach (Creature creature in allCreatures) {
                    creature.AddTask(new MoveOneRandomSquare(map, creature), false);
                }
            }

            ControlCreature();

            //reset kb state
            this.keyboardStatePrevious = Keyboard.GetState();
        }

        private void ControlCreature()
        {
            if (NewlyPressedKey(Keys.A))
            {
                controlledCreature.AddTask(new MoveOneSquare(map, Map.Direction.LEFT, controlledCreature), true); 
            }
            if (NewlyPressedKey(Keys.D))
            {
                controlledCreature.AddTask(new MoveOneSquare(map, Map.Direction.RIGHT, controlledCreature), true);
            }
            if (NewlyPressedKey(Keys.W))
            {
                controlledCreature.AddTask(new MoveOneSquare(map, Map.Direction.UP, controlledCreature), true);
            }
            if (NewlyPressedKey(Keys.S))
            {
                controlledCreature.AddTask(new MoveOneSquare(map, Map.Direction.DOWN, controlledCreature), true);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // draw mouse
            spriteBatch.Draw(this.mouseTexture, this.mousePosition, Color.White);
        }

        public MapTile GetMapTileUnderCursor()
        {
            MapTile tileUnderCursor = null;

            long cX = (long)((mousePosition.X + this.camera.GetPosition().X - this.map.GetPosition().X) / map.GetTileSize().X);
            long cY = (long)((mousePosition.Y + this.camera.GetPosition().Y - this.map.GetPosition().Y) / map.GetTileSize().Y);

            
            if (mousePosition.X < this.map.GetPosition().X -this.camera.GetPosition().X)
                cX--;
            if (mousePosition.Y < this.map.GetPosition().Y - this.camera.GetPosition().Y)
                cY--;
            
            this.debugCX = cX;
            this.debugCY = cY;

            if (this.stage.GetMap().GetTileByCoordinates(cX, cY) != null)
                tileUnderCursor = this.map.GetTileByCoordinates(cX, cY);

            return tileUnderCursor;
        }

        private bool KeyDown(Keys key)
        {
            bool keyDown = false;

            if (keyboardStateCurrent.IsKeyDown(key))
                keyDown = true;

            return keyDown;
        }

        private bool NewlyPressedKey(Keys key)
        {
            bool newlyPressed = false;

            if (this.keyboardStateCurrent.IsKeyDown(key) && this.keyboardStatePrevious.IsKeyUp(key))
                newlyPressed = true;

            return newlyPressed;
        }

        public void SetControlledCreature(Creature c) {
            this.controlledCreature = c;
        }

        public void SetAllCreatures(List<Creature> allCreatures) {
            this.allCreatures = allCreatures;
        }
    }
}
