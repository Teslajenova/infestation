﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infestation01.GameElements.Maps;
using Infestation01.GameElements.Creatures;

namespace Infestation01.Engine
{
    class MapTileOccupancyResolver
    {
        Map map;

        List<Creature> creatures;

        public MapTileOccupancyResolver(Map map, List<Creature> creatures)
        {
            this.map = map;
            this.creatures = creatures;
        }

        public Boolean TileIsOccupied(MapTile tile)
        {
            foreach(Creature creature in creatures){
                if (!creature.IsOffMap() && creature.GetCurrentTile() == tile)
                {
                    return true;
                }
            }

            return false;
        }

        public Creature GetOccupyingCreature(MapTile tile)
        {
            foreach (Creature creature in creatures)
            {
                if (!creature.IsOffMap() && creature.GetCurrentTile() == tile)
                {
                    return creature;
                }
            }

            return null;
        }
    }
}
