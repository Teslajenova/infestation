﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Infestation01;

namespace Infestation01.Engine
{
    public class GraphLib
    {

        private SpriteFont defFont;

        private Texture2D cross;
        private Texture2D wpixel;
        private Texture2D dirt;
        private Texture2D hole;
        private Texture2D shroom01;
        private Texture2D shroom02;
        private Texture2D rat02;
        private Texture2D mog;

        public GraphLib()
        { 
        }

        public void LoadContent(Game1 game)
        {
            this.defFont = game.Content.Load<SpriteFont>("Font/defFont");

            this.cross = game.Content.Load<Texture2D>("Img/cross");
            this.wpixel = game.Content.Load<Texture2D>("Img/wpixel");
            this.dirt = game.Content.Load<Texture2D>("Img/dirt");
            this.hole = game.Content.Load<Texture2D>("Img/hole");
            this.shroom01 = game.Content.Load<Texture2D>("Img/shroom01");
            this.shroom02 = game.Content.Load<Texture2D>("Img/shroom02");
            this.rat02 = game.Content.Load<Texture2D>("Img/rat02");
            this.mog = game.Content.Load<Texture2D>("Img/mog");
        }

        public SpriteFont GetDefFont() { return this.defFont; }

        public Texture2D GetCross() { return this.cross; }
        public Texture2D GetWpixel() { return this.wpixel; }
        public Texture2D GetDirt() { return this.dirt; }
        public Texture2D GetHole() { return this.hole; }
        public Texture2D GetShroom01() { return this.shroom01; }
        public Texture2D GetShroom02() { return this.shroom02; }
        public Texture2D GetRat02() { return this.rat02; }
        public Texture2D GetMog() { return this.mog; }

        

    }
}
