﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Infestation01.Engine
{
    class Setup
    {

        Stage stage1;

        Input input;
        Camera camera;

        SpriteFont debugFont;


        public Setup(GraphLib graphLib, float screenSizeX, float screenSizeY)
        {
            this.stage1 = new Stage(graphLib, screenSizeX, screenSizeY);

            this.camera = new Camera(new Vector2(150f), 5f);

            this.input = new Input(this.stage1, this.camera, this.stage1.GetMap(), graphLib);

            input.SetControlledCreature(stage1.GetCreatures().ElementAt(0));
            input.SetAllCreatures(stage1.GetCreatures());

            this.debugFont = graphLib.GetDefFont();
        }

        public void Update(GameTime gameTime)
        {
            this.input.Update();
            this.stage1.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            this.stage1.Draw(spriteBatch, this.camera);

            this.input.Draw(spriteBatch);


            spriteBatch.DrawString(this.debugFont, 
                this.input.debugCX.ToString() + "/" + this.input.debugCY.ToString(), 
                new Vector2(20f), Color.Yellow);
            /*
            spriteBatch.DrawString(this.debugFont, 
                "cX:" + this.camera.GetPosition().X + "/cY:" + this.camera.GetPosition().Y, 
                new Vector2(20f), Color.White);
            */
        }
    }
}
