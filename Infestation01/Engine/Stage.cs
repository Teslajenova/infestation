﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Infestation01.Engine;
using Infestation01.GameElements.Maps;
using Infestation01.GameElements.Creatures;
using Infestation01.GameElements.Ai;
using Infestation01.GameElements.Items;

namespace Infestation01.Engine
{
    class Stage
    {

        //Input input;

        Map map;

        List<Creature> creatures;

        private List<Item> allItems;

        Vector2 screenSize;

        int itemUpKeepCounterMax = 1000 * 60 * 5;
        int itemUpkeepCounter;

        public Stage(GraphLib graphLib, float screenSizeX, float screenSizeY)
        {
            //this.input = input;

            this.itemUpkeepCounter = itemUpKeepCounterMax;
            this.screenSize = new Vector2(screenSizeX, screenSizeY);

            Vector2 tileSize = new Vector2(30f, 30f);

            this.creatures = new List<Creature>();
            this.allItems = new List<Item>();
            this.map = new Map(tileSize, new Vector2(400f), 200, graphLib, this.creatures, allItems);

            for (int i = 0; i < 10; i++)
            {
                //this.map.GetRandomTile(MapTile.TileType.OPEN).SetTileType(MapTile.TileType.EXIT);
            }

            this.InitCreatures(graphLib);

        }

        private void InitCreatures(GraphLib graphLib)
        {    
            //this.creatures.Add(new Rat(this.map.GetRandomTile(MapTile.TileType.OPEN), this.map, graphLib));
            
            //this.creatures.Add(new Moogle(this.map.GetRandomTile(MapTile.TileType.OPEN), this.map, graphLib));

            for (int i = 0; i < 60; i++)
            {
                Creature mog = new Moogle(this.map.GetRandomTile(MapTile.TileType.OPEN), this.map, graphLib, allItems);
                //Creature mog = new Moogle(this.map.GetTileByCoordinates(0,0), this.map, graphLib);
                //mog.AddTask(new RunAround(map, mog));
                
                /*
                List<MapTile> goals = new List<MapTile>();
                goals.Add(map.GetTileByCoordinates(0,0));
                mog.AddTask(new GotoTile(map,goals,mog));
                 * */
                //mog.AddTask(new GoOut(mog, map),false);
                //mog.AddTask(new MoveOneRandomSquare(map, mog), true);

                this.creatures.Add(mog);
            }

            String a = "A";
   
        }

        public Map GetMap()
        {
            return this.map;
        }

        public List<Creature> GetCreatures() {
            return this.creatures;
        }

        public void Update(GameTime gameTime)
        {
            foreach (Creature creature in this.creatures)
            {
                creature.Update(gameTime);
            }

            this.map.Update(gameTime);

            ItemUpkeep(gameTime);
        }

        private void ItemUpkeep(GameTime gameTime) {
            itemUpkeepCounter -= (gameTime.ElapsedGameTime.Milliseconds);

            if (itemUpkeepCounter < 0) {
                allItems.RemoveAll(item => item.GetHolder() == null);

                itemUpkeepCounter = itemUpKeepCounterMax;
            }
        }

        public void Draw(SpriteBatch spriteBatch, Camera camera)
        {
            this.map.Draw(spriteBatch, camera);

            foreach (Creature creature in this.creatures)
            {
                creature.Draw(spriteBatch, camera);
            }
        }

    }
}
